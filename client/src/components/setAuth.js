import React, { Component } from 'react'

import { withRouter } from 'react-router';
//import './RecoverPassword.css'
import { authorize } from './UserFunctions'
import { Form, Message, Button } from 'semantic-ui-react'

//Login Page TODO make it pretty
class setAuth extends Component {
    constructor() {
        super()
        this.state = {
            count:5,
            loaded: false,
            failed: false,
            succeeded:false,
        }

    }

    componentDidMount() {
        this.setState({ loaded: true })
        authorize()
        .then(response => {
            console.log(response)
            this.setState({ loaded: false })
            this.setState({ succeeded: true })
        })
        .catch(err => {
            console.log("Authorization had a little problem: " + err)
            this.setState({ loaded: false })
            this.setState({ failed: true })
            
            
        })
        this.inter = setInterval(() => {
           
            if (this.state.count <= 0) {
                if(this.state.succeeded){
                    this.props.history.push('/login')
                }
            } else {
              this.setState((prevState) => ({count: prevState.count - 1})); 
            }
          }, 1000);
        

    }

    render() {
        localStorage.setItem('authToken',this.props.match.params.token)
        return (
            <div className="contain">
                <div className="col-md-6 mt-5 mx-auto">
                    <Form warning={this.state.loaded} success={this.state.succeeded} error={this.state.failed}>
                        <Message warning >
                            <Message.Header>Autorização está a ser efetuada</Message.Header>
                            <Message.Content> Será redireccionado uma vez que o processo termine
                                </Message.Content>
                        </Message>
                        <Message success >
                            <Message.Header>Autorização efetuada com sucesso</Message.Header>
                            <Message.Content> Será redireccionado para o login
                                </Message.Content>
                        </Message>
                        <Message error >
                            <Message.Header>Autorização falhou :(</Message.Header>
                            <Message.Content> Será tentado novamente este processo 
                                </Message.Content>
                        </Message>
                    </Form>
                            <div>Redireccionado em : {this.state.count}</div>

                </div>

            </div>

        )
    }
}

export default withRouter(setAuth)