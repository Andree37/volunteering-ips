import React, { Component } from 'react'

import { withRouter } from 'react-router';
//import './RecoverPassword.css'
import { recover, sendAuthMail } from './UserFunctions'
import { Form, Message, Button } from 'semantic-ui-react'

//Login Page TODO make it pretty
class Authorization extends Component {
    constructor() {
        super()
        this.state = {
            email: '',
            emailError: false,
            emailSent: false,
            isRegisted: false,
            isSending: false
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    //Handy workaround for target values on change
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }
    //redefines submit method to implement UserFunction/login redirects to profile on ok
    onSubmit(e) {
        e.preventDefault()
        this.setState({ emailError: false })
        this.setState({ emailSent: false })
        this.setState({ isSending: true })
        const recoverMail = {
            email: this.state.email
        }

        sendAuthMail(recoverMail).then(response => {
            if (response) {
                this.setState({ emailSent: true })

            } else {
                this.setState({ emailError: true })
            }
            this.setState({ isSending: false })
        })
    }
    componentDidMount() {
        this.state.email = localStorage.getItem('email')
        if(this.state.email!==""){
            this.setState({isRegisted:true})
        }else{
            this.setState({isRegisted:false})
        }
    }

    render() {
       



        return (
            <div className="contain">
                    <div className="col-md-6 mt-5 mx-auto">
                        <Form loading={this.state.isSending} success={this.state.emailSent} warning={this.state.isRegisted} error={this.state.emailError} onSubmit={this.onSubmit}>
                        <Message warning>
                            <Message.Header>Bem Vindo!</Message.Header>
                            <Message.List>
                                <Message.Item>Abra o link enviado para si via email, para autenticar a sua conta.</Message.Item>
                                <Message.Item>Para efetuar um login com sucesso terá que ter a sua conta autenticada </Message.Item>
                                <Message.Item>O seu registo foi completado com sucesso! Um email foi enviado para : {this.state.email}</Message.Item>
                            </Message.List>
                        </Message>
                        <Message error>
                            <Message.Header>Email não foi enviado :(</Message.Header>
                            <Message.List>
                                <Message.Item>Algo não correu bem... aguarde ou tente mais tarde .</Message.Item>
                                </Message.List>
                        </Message>
                        
                        <text>Caso não tenha recebido um email, aguarde uns minutos ou então pressione o botão em baixo:</text>
                        <br></br>
                        <Button type="submit" >Enviar mail</Button>
                        <Message success>
                            <Message.Header>Email foi enviado :)</Message.Header>
                            
                        </Message>
                        </Form>
                    

                </div>

            </div>

        )
    }
}

export default withRouter(Authorization)