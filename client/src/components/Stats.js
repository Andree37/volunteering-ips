import React, { Component } from "react";
import { ColumnChart, PieChart } from '@opd/g2plot-react'
import '@antv/g2plot'
import axios from 'axios'
import { Label } from "semantic-ui-react";

//This file is to translate API calls to functions
// pod "http://pvipsback-service.pvips-dev.svc.cluster.local/"
//local "http://localhost:8000"
//"proxy": "http://35.189.210.245:80/",

export function getStats() {
  return axios
    .get('statistics')
}

//Project where we can put stuff
class Stats extends Component {
  constructor() {
    super()
    this.state = {
      gotten: false,
      avg_users_age: 0,
      avg_project_completion: 0,
      avg_users_month: 0,
      num_projects: 0,
      num_users: 0,
      entities_envolved: [{ type: "a", value: 0 }],
      interest_areas: [{ type: "a", value: 0 }],
      intervention_area: [{ type: "a", value: 0 }],
      project_type: [{ type: "a", value: 0 }],
      specific_formation: [{ type: "a", value: 0 }],
      users_ages: [{ type: "a", value: 0 }],
      users_month: [{ type: "a", value: 0 }],
    }
  }
  componentWillMount() {
    getStats().then(res => {
      console.log(res.data.result)
      var data = res.data.result
      this.setState({ avg_users_age: data.avg_users_age })
      this.setState({ avg_project_completion: data.avg_project_completion })
      this.setState({ avg_users_month: data.avg_users_month })
      this.setState({ num_projects: data.num_projects })
      this.setState({ num_users: data.num_users })

      this.setState({ entities_envolved: data.entities_envolved }) //pie
      this.setState({ interest_areas: data.interest_areas }) //pie
      this.setState({ intervention_area: data.intervention_area }) //pie 
      this.setState({ specific_formation: data.specific_formation }) //pie

      this.setState({ project_type: data.project_type }) //column
      this.setState({ users_ages: data.users_ages })//column
      this.setState({ users_month: data.users_month })//column
      this.setState({gotten : true})
    })
  }

  getGraphs(){
   
     
      const configEntities = {
        forceFit: true,
        title: {
          visible: true,
          text: "Entidates envolvidas"
        },
        description: {
          visible: true,
          text: ""
        },
        radius: 0.8,
        data: this.state.entities_envolved,
        angleField: "value",
        colorField: "type",
        label: {
          visible: true,
          type: "inner"
        }
      }
      const configInterest = {
        forceFit: true,
        title: {
          visible: true,
          text: "Áreas de interesse"
        },
        description: {
          visible: true,
          text: ""
        },
        radius: 0.8,
        data: this.state.interest_areas,
        angleField: "value",
        colorField: "type",
        label: {
          visible: true,
          type: "inner"
        }
      }
      const configIntervention = {
        forceFit: true,
        title: {
          visible: true,
          text: "Áreas de intervenção"
        },
        description: {
          visible: true,
          text: ""
        },
        radius: 0.8,
        data: this.state.intervention_area,
        angleField: "value",
        colorField: "type",
        label: {
          visible: true,
          type: "inner"
        }
      }
      const configFormation = {
        forceFit: true,
        title: {
          visible: true,
          text: "Formação especifica"
        },
        description: {
          visible: true,
          text: ""
        },
        radius: 0.8,
        data: this.state.specific_formation,
        angleField: "value",
        colorField: "type",
        label: {
          visible: true,
          type: "inner"
        }
      }
  
  
  
      const configProjectType = {
        title: {
          visible: true,
          text: 'Tipo de projeto',
        },
        forceFit: true,
        data: this.state.project_type,
        padding: 'auto',
        xField: 'type',
        yField: 'value',
        meta: {
          type: {
            alias: 'Tipo',
          },
          value: {
            alias: 'Valor',
          },
        },
      }
  
      const configUsersAges = {
        title: {
          visible: true,
          text: 'Idades de utilizadores',
        },
        forceFit: true,
        data: this.state.users_ages,
        padding: 'auto',
        xField: 'type',
        yField: 'value',
        meta: {
          type: {
            alias: 'Idade',
          },
          value: {
            alias: 'Quantidade',
          },
        },
      }
  
      const configUserMonths = {
        title: {
          visible: true,
          text: 'Utilizadores por mês',
        },
        forceFit: true,
        data: this.state.users_month,
        padding: 'auto',
        xField: 'type',
        yField: 'value',
        meta: {
          type: {
            alias: 'Mês',
          },
          value: {
            alias: 'Quantidade',
          },
        },
      }
    
      return (<div><section>
        <PieChart {...configEntities} />
      </section>
        <section>
          <PieChart {...configFormation} />
        </section>
        <section>
          <PieChart {...configIntervention} />
        </section>
        <section>
          <PieChart {...configInterest} />
        </section>
        <section>
          <ColumnChart {...configProjectType} />
        </section>
        <section>
          <ColumnChart {...configUserMonths} />
        </section>
        <section>
          <ColumnChart {...configUsersAges  }/>
        </section></div>)
         

    
  
    
  }
  render() {
    const graphs = (this.state.gotten) ? this.getGraphs(): null;
   
    return (
      <div class="container-div">
        <h2>Dashboard de Estatísticas</h2>
        <section>
          <Label>Número de utilizadores</Label><text>{this.state.num_users}</text>
          <br></br>
          <Label>Média das idades de utilizadores</Label><text>{this.state.avg_users_age}</text>
          <br></br>
          <Label>Média de utilizadores por mês</Label><text>{this.state.avg_users_month}</text>
          <br></br>
          <Label>Número de projetos</Label><text>{this.state.num_projects}</text>
          <br></br>
          <Label>Média de projetos concluidos</Label><text>{this.state.avg_project_completion}</text>
          <br></br>
        </section>
        {graphs}
      </div>
    );
  }
}

export default Stats;