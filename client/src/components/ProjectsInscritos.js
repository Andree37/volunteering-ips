import React, {Component} from'react'
import './ProjectsInscritos.css'

export default class ProjectsInscritos extends Component{
    render(){
        return(
            <div class="insc-container">
                <h1 class="h1-insc">Inscritos no Projeto</h1>
                    <table id="t01" class="inscritos">
                    <tr>
                        <th>Nome do Utilizador</th>
                        <th>Tipo de Membro do IPS</th> 
                        <th>Escola/Serviço</th>
                        <th>Email</th>
                        <th class="remover">Remover</th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="remover"><button>Remover</button></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="remover"><button>Remover</button></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="remover"><button>Remover</button></td>
                    </tr>
                </table>
            </div>
        )
    }
}