



//"/users/recover_password/{token}"

import React, { Component } from 'react'
import {withRouter} from 'react-router';

import { Form, FormGroup, Image, Button, Message } from 'semantic-ui-react'
import { resetUserPassword } from './UserFunctions'
import { recover } from './UserFunctions'


//Login Page TODO make it pretty
class ResetPassword extends Component {
    constructor() {
        super()
        this.state = {
            password: '',
            confirmPassword: '',
            isLoading: false,
            isInvalid: false
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    //Handy workaround for target values on change
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }
    hasValidFormData() {
        if (this.state.password === this.state.confirmPassword) {
            return true;
        }else{
            return false
        }
    }
    //redefines submit method to implement UserFunction/login redirects to profile on ok
    onSubmit(e) {
        e.preventDefault()
        
        if (this.hasValidFormData()) {
            this.setState({isInvalid:false})
            this.setState({isLoading:true})
            const userUpdate = {
                password: this.state.password

            }
            resetUserPassword(userUpdate).then(res => {
                if (res) {
                    this.props.history.push('/profile')

                }else{
                    alert("Backend broke")
                    

                }
                this.setState({isLoading:false})
            })
        }else{
            this.setState({isInvalid:true})
        }
    }

    render() {
        localStorage.setItem('resetToken',this.props.match.params.token)
        return (
            <div className="contain">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <Form loading={this.state.isLoading} error={this.state.isInvalid}>
                        <Message
                                error
                                header='Reset Inválido'
                                content='Palavra(s) Chave(s) inválidas(s)/não correspondem'
                            />
                            <FormGroup>
                                <Form.Field>
                                    <Form.Input
                                        required
                                        type="password"
                                        label="Password"
                                        placeholder="pass_123ABC"
                                        name="password"
                                        value={this.state.password}
                                        onChange={this.onChange}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <Form.Input
                                        required
                                        type="password"
                                        label="Confirmar Password"
                                        placeholder="pass_123ABC"
                                        name="confirmPassword"
                                        value={this.state.confirmPassword}
                                        onChange={this.onChange}
                                    />
                                </Form.Field>
                            </FormGroup>
                            <Form.Button className="btn" onClick={this.onSubmit}>Alterar</Form.Button>
                        </Form>

                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ResetPassword)