import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { logout } from './UserFunctions'
import { getNotify } from './NotificationFunctions'
import './Navbar.css'


//Sets the navbar and readjusts on login/logout accesses
class Navbar extends Component {
    constructor() {
        super()
        this.state = {
            profileCounter: 0,
            profileNotifications:[],
            projectCounter:0,
            projectNotifications:[],

        }
        window.navbarComponent = this;
    }
    //Sends to Homepage, but doesnt logout from API cuz bad token
    logOut(e) {
        e.preventDefault()
        localStorage.removeItem('userToken')
        logout();
        this.props.history.push('/')
    }
    getProjectNotification(id){
        var notification 
        this.state.projectNotifications.forEach(element => {
            var contents = element.content.split(":");
            if(contents[0].includes(id)){
                notification = element.type
            }
        });
        return notification
    }

    projectHasNotification(id){
        var has = false
        this.state.projectNotifications.forEach(element => {
            var contents = element.content.split(":");
            if(contents[0].includes(id)){
                has = true
            }
        });
        return has
    }
    updateNotifications(){
        this.setState({profileCounter:0})
            getNotify().then(res => {
                if(!res.data.error){
                localStorage.setItem("ProfileNotifications", null)
                var notificationArray = res.data.result.notifications
                var newProfileNotify='';
                var count = this.state.profileCounter;
                var commissionProjectNotificationsArray = [];
                notificationArray.forEach(el => {
                    console.log("New Notification :"+ el.type + el.seen)      
                    var touple = el.type.split(":")         
                    if (el.type === "Profile" && !el.seen) {  
                        count++
                        this.setState({profileCounter:count})
                        this.state.profileNotifications.push(el)
                        newProfileNotify = el.type
                        
                    }else if(localStorage.getItem("userToken")=="dnl.afonso1@gmail.com" && touple[0] === "ProjectApproval"  && !el.seen){
                        commissionProjectNotificationsArray.push(el)
                    }
                }
                );
                this.setState({projectNotifications: commissionProjectNotificationsArray})
            }
                
            });

        
    }
    //When render
    componentDidMount() {
        if (localStorage.getItem('userToken')!= null) {
            this.updateNotifications()
        }
    }
    


    //Ugly-ish looking bar
    render() {

        //notify badges
        const showProfileNotify =  (this.state.profileNotifications.length >0 )? <div className="dot">
            {this.state.profileNotifications.length}
        </div>: null ; 
         const showProjectNotify =  (this.state.projectNotifications.length >0 )? <div className="dot">
         {this.state.projectNotifications.length}
         </div>: null ; 


        //with no login access
        const loginRegLink = (
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to="/login">Login</Link>

                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/register">Registar</Link>

                </li>
            </ul>

        )
        //logged in access
        const userLink = (
            
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to="/Profile">Perfil
                     
                    </Link>
                   
                    

                </li>
                {showProfileNotify}
                <li className="nav-item">
                    <a href="/logout" onClick={this.logOut.bind(this)} className="nav-link"> Logout</a>

                </li>
            </ul>
            
           
        )
        //Some more ugly stuff
        return (
            
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <button className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbar1"
                    aria-controls="navbar1"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-begining" id="navbar2">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/" className="nav-link pvips">
                                PVIPS
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="collapse navbar-collapse justify-content-end" id="navbar1">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/" className="nav-link">
                                Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/projects" className="nav-link">
                                Projetos
                            </Link>
                            
                        </li>
                        {showProjectNotify}
                        
                        <li className="nav-item">
                            <Link to="/stats" className="nav-link">
                                Estatísticas
                            </Link>
                        </li>

                    </ul>
                    {localStorage.userToken ? userLink: loginRegLink}
                </div>

            </nav>

        )
    }
}

export default withRouter(Navbar)

