import axios from 'axios'

//This file is to translate API calls to functions
// pod "http://pvipsback-service.pvips-dev.svc.cluster.local/"
//local "http://localhost:8000"
//"proxy": "http://35.189.210.245:80/",

//get Notifications from a logged user
export function getNotify(){
    return axios
    .get('notify/')
    .then(res=>{
        return res
    })
    .catch(err => {
        console.log("Register user had a little problem: "+err)
        return err
    })
}

export function postOtherNotify(notification){
    return axios
    .post('notifyOther/',{
        email:notification.email,
        content:notification.content,
        type:notification.type
    })
    .then(res=>{
        return res
    })
    .catch(err => {
        console.log("Notify user had a little problem: "+err)
        return err
    })
}

export function readNotification(notification){
    return axios
    .put('notify/', {
        type:notification.type
    }).then(res=>{
        return res
    })
    .catch(err => {
        console.log("Reading notify user had a little problem: "+err)
        return err
    })

}