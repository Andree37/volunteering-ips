import React, { Component } from 'react'
import { Form, FormGroup, Message } from 'semantic-ui-react'
import { register } from './UserFunctions'
import './Register.css'
import { postOtherNotify } from './NotificationFunctions';
import { wait } from '@testing-library/react';


//Register Page
export default class Register extends Component {
    constructor() {
        super();
        this.state = {
            name: "",
            email: "",
            phonenumber: "",
            password: "",
            confirmPassword: "",
            is_member: false,
            type_member: "",
            birthdate: "1998-05-18",
            school: "",
            course: "",
            interest_areas: "",
            reason_volunteer: "",
            observations: "",
            isLoading: false,
            isSuccess: false,
            consent: false,
            emailExists: false,
        }


        //Substituite the submit method
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    //Handy simplify change values of target
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
        console.log("Changed: " + e.target.name + " to " + e.target.value)
    }

    //HALF-TODO Some form of validation
    hasValidFormData() {
        if (this.state.password === this.state.confirmPassword) {
            if (this.state.consent) {
                return true;
            } else {
                alert("Por favor aceite a autorização RGPD");
                return false;
            }

        } else {
            return false
        }
    }
    //state change for checkbox
    toggle = () => {

        this.setState((state) => ({ is_member: !state.is_member }))
        this.setState({ type_member: "" })
        this.setState({ birthdate: "" })
        this.setState({ school: "" })
        this.setState({ course: "" })
        this.setState({ interest_areas: "" })
        this.setState({ reason_volunteer: "" })
        this.setState({ observations: "" })

    }
    toggleConsent = () => {

        this.setState((state) => ({ consent: !state.consent }))


    }
    optChangeType = (e, { value }) => {
        this.state.type_member = value
        console.log("Changed: " + this.state.type_member + " to " + value)

    }
    optChangeReason = (e, { value }) => {
        this.state.reason_volunteer = value
        console.log("Changed: " + this.state.reason_volunteer + " to " + value)
    }
    optChangeAreas = (e, { value }) => {
        this.state.interest_areas = value
        console.log("Changed: " + this.state.interest_areas + " to " + value)
    }
    optChangeSchool = (e) => {
        this.state.school = e.target.options[e.target.selectedIndex].text
        console.log("Changed: " + this.state.school + " to " + e.target.options[e.target.selectedIndex].text)
    }
    optChangeCourse = (e) => {
        this.state.course = e.target.options[e.target.selectedIndex].text
        console.log("Changed: " + this.state.course + " to " + e.target.options[e.target.selectedIndex].text)
    }

    //Captures the submit event and sends to API
    onSubmit(event) {
        localStorage.setItem('email', null)
        if (this.hasValidFormData()) {
            // this.registerNewUser();
           
            this.setState({ emailExists: false })
            this.setState({ isLoading: true })

            const newUser = {
                name: this.state.name,
                email: this.state.email,
                phonenumber: this.state.phonenumber,
                password: this.state.password,
                is_member: this.state.is_member,
                type_member: this.state.type_member,
                birthdate: this.state.birthdate,
                school: this.state.school,
                course: this.state.course,
                interest_areas: this.state.interest_areas,
                reason_volunteer: this.state.reason_volunteer,
                observations: this.state.observations
            }
            this.setState({ isSuccess: true })
            if(!newUser.is_member){
                newUser.birthdate = '1998-05-18';
            }
            //registers actual user
            register(newUser).then(res => {
                console.log(res.data.result.email.toString())
                if (res.data.result.email.toString().includes('already exists')) {
                    this.setState({ isLoading: false })
                    this.setState({ emailExists: true })
                } else {
                    //sends welcome profile notification
                    const Notification = {
                        email: this.state.email,
                        content: "User created",
                        type: "Profile"
                    }
                    postOtherNotify(Notification).then(res => {
                        localStorage.setItem('email', this.state.email)
                        this.props.history.push('/Authorization')

                    })
                }
            })
        } else {
            this.setState({ isLoading: false })
        }

    }

    //Renders UI
    render() {
        var today = new Date();
        today.setFullYear(2001)
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;


        const memberOptions = [
            { key: 'e', text: 'Estudante', value: 'Estudante' },
            { key: 'd', text: 'Docente', value: 'Docente' },
            { key: 'nd', text: 'Não Docente', value: 'Não Docente' },
            { key: 'di', text: 'Diplomado', value: 'Diplomado' },
            { key: 'b', text: 'Bolseiro', value: 'Bolseiro' },
            { key: 'a', text: 'Aposentado', value: 'Aposentado' }

        ]

        const interestOptions = [
            { key: 'aa', text: 'Atividades Académicas', value: 'Atividades Académicas' },
            { key: 'am', text: 'Ambiental', value: 'Ambiental' },
            { key: 'ae', text: 'Apoio a Eventos', value: 'Apoio a Eventos' },
            { key: 'it', text: 'Informática', value: 'Informática' },
            { key: 'c', text: 'Comunicação', value: 'Comunicação' },
            { key: 'ct', text: 'Cultural', value: 'Cultural' },
            { key: 'de', text: 'Desporto', value: 'Desporto' },
            { key: 'ed', text: 'Educação', value: 'Educação' },
            { key: 'sa', text: 'Saúde', value: 'Saúde' },
            { key: 'so', text: 'Social', value: 'Social' }

        ]

        const voluntaryOptions = [
            { key: 'cs', text: 'Convívio social', value: 'Convívio social' },
            { key: 'ppp', text: 'Porque pode ser vantajoso para o futuro profissional ', value: 'Porque pode ser vantajoso para o futuro profissional ' },
            { key: 'ppis', text: 'Pela possibilidade de integração social ', value: 'Pela possibilidade de integração social ' },
            { key: 'pton', text: 'Para ter novas experiências', value: 'Para ter novas experiências' },
            { key: 'posd', text: 'Porque gosto de ajudar os outros ', value: 'Porque gosto de ajudar os outros ' },
            { key: 'pfof', text: 'Porque fui incentivado(a) por outras pessoas', value: 'Porque fui incentivado(a) por outras pessoas' },
            { key: 'pne', text: 'Para ter novas experiências', value: 'Para ter novas experiências' },
            { key: 'pcoc', text: 'Porque conheço pessoas que já realizaram atividades de voluntariado no IPS', value: 'Porque conheço pessoas que já realizaram atividades de voluntariado no IPS' },
            { key: 'pmsu', text: 'Para me sentir útil ', value: 'Para me sentir útil ' }
        ]



        const content = this.state.is_member

            ? <div>
                <Form.Field>
                    <Form.Input
                        label="Data de nascimento"
                        type="date"
                        name="birthdate"
                        required
                        max={today}
                        value={this.state.birthdate}
                        onChange={this.onChange}
                    />
                </Form.Field>
                <Form.Field>
                    <Form.Select
                        label='Tipo de membro'
                        placeholder='Escolha tipo de membro'
                        name="type_member"
                        required
                        onChange={this.optChangeType}
                        fluid
                        options={memberOptions}
                    />

                </Form.Field>
                <Form.Field required>
                    <label >Escola/Serviço</label>
                    <select name="school" onChange={this.optChangeSchool} >
                        <option value="" selected disabled hidden>Escolha aqui</option>
                        <option value="ESTS">ESTS - Escola Superior de Tecnologia de Setúbal</option>
                        <option value="ESE">ESE - Escola Superior de Educação</option>
                        <option value="ESCE">ESCE - Escola Superior de Ciências Empresariais</option>
                        <option value="ESTB">ESTB- Escola Superior de Tecnologia do Barreiro</option>
                        <option value="ESS">ESS - Escola Superior de Saúde</option>
                        <option value="219">ADIR - ADIR - Assessoria da Direção</option>
                        <option value="214">GCPSE - APSE - Apoio à Prestação de Serviços Especializado</option>
                        <option value="167">BIB - BIB - Biblioteca</option>
                        <option value="158">DEE - DEE - Departamento de Eng.ª Eletrotécnica</option>
                        <option value="157">DEM - DEM - Departamento de Eng.ª Mecânica</option>
                        <option value="156">DMAT - DMAT - Departamento de Matemática</option>
                        <option value="155">DSI - DSI - Departamento de Sistemas e Informática</option>
                        <option value="213">E+ - E+ - Gabinete de Apoio ao Estudante l Paul Graham</option>
                        <option value="180">EGS - EGS - Economato e Gestão de Stocks</option>
                        <option value="215">GAAL - GAAL - Gabinete de Apoio à Atividade Letiva</option>
                        <option value="203">GAE - GAE - Gabinete de Apoio ao E-Learning</option>
                        <option value="175">GIP - GIP - Gabinete de Integração Profissional</option>
                        <option value="169">Manut - MANUT - Manutenção</option>
                        <option value="154">SACEC - SACEC - Secção Autónoma de Ciências Empresariais e</option>
                        <option value="218">SDEP - SDEP - Secretariado dos Departamentos</option>
                        <option value="217">SOG - SOG - Secretariado dos Órgãos de Gestão</option>
                        <option value="220">SSI - SSI - Suporte ao Sistema de Informação</option>
                    </select>
                </Form.Field>
                <Form.Field>
                    <label>Curso/Formação</label>
                    <select name="course" onChange={this.optChangeCourse}>
                        <option value="" selected disabled hidden>Escolha aqui</option>
                        <optgroup label="Formação">
                            <option value="34">Não Definido</option>
                            <option value="35">Ensino Básico (9º Ano)</option>
                            <option value="36">Ensino Secundário - Incompleto</option>
                            <option value="37">Ensino Secundário Completo</option>
                            <option value="38">Curso Tecnico-Profissional (Equivalência Ensino Secundário)</option>
                            <option value="39">Formação</option>
                            <option value="40">Propdêutico</option>
                            <option value="41">Bacharelato</option>
                            <option value="42">Licenciatura</option>
                            <option value="43">Pós-Graduação</option>
                            <option value="44">Mestrado</option>
                            <option value="45">Doutoramento</option>
                            <option value="46">CET</option>
                            <option value="47">MBA</option>
                            <option value="48">CTeSP</option>
                            <option value="49">Licenciatura - Incompleto</option>
                            <option value="50">Mestrado - Incompleto</option>
                        </optgroup>
                        <optgroup label="Cursos Activos">
                            <option value="XT">XT - Alunos Extraordinários </option>
                            <option value="AP">AP - Ano Preparatório </option>
                            <option value="CCDIID">CCDIID - Curso de Curta Duração - Instrumentação Industrial - Diurno </option>
                            <option value="CCDIIL">CCDIIL - Curso de Curta Duração - Instrumentação Industrial - Pós-Laboral </option>
                            <option value="CI">CI - Curso de Inglês </option>
                            <option value="TSPARC">TSPARC - Curso Técnico Superior Profissional em Automação, Robótica e Controlo Industrial </option>
                            <option value="ARCIL">ARCIL - Curso Técnico Superior Profissional em Automação, Robótica e Controlo Industrial </option>
                            <option value="TSPCE">TSPCE - Curso Técnico Superior Profissional em Climatização e Energia </option>
                            <option value="APIEF">APIEF - Curso Técnico Superior Profissional em Climatização e Energia - APIEF </option>
                            <option value="DVAM">DVAM - Curso Técnico Superior Profissional em Desenvolvimento de Videojogos e Aplicaçõe </option>
                            <option value="TSPE">TSPE - Curso Técnico Superior Profissional em Eletromedicina </option>
                            <option value="CINEL">CINEL - Curso Técnico Superior Profissional em Eletromedicina - CINEL (Lisboa) </option>
                            <option value="TSPGAS">TSPGAS - Curso Técnico Superior Profissional em Gestão do Ambiente e Segurança </option>
                            <option value="TSPIE">TSPIE - Curso Técnico Superior Profissional em Instalações Elétricas </option>
                            <option value="TSPMI">TSPMI - Curso Técnico Superior Profissional em Manutenção Industrial </option>
                            <option value="TSPFAC">TSPFAC - Curso Técnico Superior Profissional em Modelação e Fabrico Assistidos por Comput </option>
                            <option value="TSPOGI">TSPOGI - Curso Técnico Superior Profissional em Organização e Gestão Industrial </option>
                            <option value="TSPPA">TSPPA - Curso Técnico Superior Profissional em Produção Aeronáutica </option>
                            <option value="GAIR">GAIR - Curso Técnico Superior Profissional em Produção Aeronáutica - Centro Aeronáutico </option>
                            <option value="TSPAG">TSPAG - Curso Técnico Superior Profissional em Produção Aeronáutica - Grândola </option>
                            <option value="TSPCDA">TSPCDA - Curso Técnico Superior Profissional em Programação Web, Dispositivos e Aplicaçõe </option>
                            <option value="QAA">QAA - Curso Técnico Superior Profissional em Qualidade Ambiental e Alimentar </option>
                            <option value="TSPPAR">TSPPAR - Curso Técnico Superior Profissional em Redes e Sistemas Informáticos </option>
                            <option value="REID">REID - Curso Técnico Superior Profissional em Redes Elétricas Inteligentes e Domótica </option>
                            <option value="TSPSEC">TSPSEC - Curso Técnico Superior Profissional em Sistemas Eletrónicos e Computadores </option>
                            <option value="IPESEC">IPESEC - Curso Técnico Superior Profissional em Sistemas Eletrónicos e Computadores - IPE </option>
                            <option value="TSPTGA">TSPTGA - Curso Técnico Superior Profissional em Tecnologia e Gestão Automóvel </option>
                            <option value="TSPTB">TSPTB - Curso Técnico Superior Profissional em Tecnologias e Programação de Sistemas de  </option>
                            <option value="TSPTSI">TSPTSI - Curso Técnico Superior Profissional em Tecnologias e Programação de Sistemas de  </option>
                            <option value="IPE">IPE - Curso Técnico Superior Profissional em Tecnologias e Programação de Sistemas de  </option>
                            <option value="TPSIB">TPSIB - Curso Técnico Superior Profissional em Tecnologias e Programação de Sistemas de  </option>
                            <option value="TINFT">TINFT - Curso Técnico Superior Profissional em Tecnologias Informáticas (Turma do Progra </option>
                            <option value="TSPVE">TSPVE - Curso Técnico Superior Profissional em Veículos Elétricos </option>
                            <option value="ERS">ERS - Erasmus </option>
                            <option value="FC">FC - Formação Complementar </option>
                            <option value="FPIF">FPIF - Formação Pedagógica Inicial de Formadores </option>
                            <option value="EACI">EACI - Licenciatura em Engenharia de Automação, Controlo e Instrumentação </option>
                            <option value="EEC">EEC - Licenciatura em Engenharia Eletrotécnica e de Computadores </option>
                            <option value="INF">INF - Licenciatura em Engenharia Informática </option>
                            <option value="EM">EM - Licenciatura em Engenharia Mecânica </option>
                            <option value="LTB">LTB - Licenciatura em Tecnologia Biomédica </option>
                            <option value="TGI">TGI - Licenciatura em Tecnologia e Gestão Industrial </option>
                            <option value="LTE">LTE - Licenciatura em Tecnologias de Energia </option>
                            <option value="LTAM">LTAM - Licenciatura em Tecnologias do Ambiente e do Mar </option>
                            <option value="MP">MP - Mestrado em Engenharia de Produção </option>
                            <option value="MES">MES - Mestrado em Engenharia de Software </option>
                            <option value="MEGEIE">MEGEIE - Mestrado em Engenharia e Gestão de Energia na Indústria e Edifícios </option>
                            <option value="MEEC">MEEC - Mestrado em Engenharia Eletrotécnica e de Computadores </option>
                            <option value="MIG">MIG - Mestrado em Informática de Gestão </option>
                            <option value="M23">M23 - Mini curso de  preparação para os maiores de 23 anos </option>
                            <option value="PGMVEH">PGMVEH - Pós-Graduação em  Motorização de Veículos Elétricos e Híbridos </option>
                            <option value="PG EI">PG EI - Pós-Graduação em Engenharia Informática </option>
                            <option value="PGTAE">PGTAE - Pós-Graduação em Tecnologia Aeronáutica </option>
                            <option value="TA">TA - Pós-Graduação em Tecnologia Aeronáutica </option>
                            <option value="TDCP">TDCP - Transformação Digital Centrada nas Pessoas </option>
                            <option value="UCMOB">UCMOB - UC Mobilidade </option>
                            <option value="VG">VG - Vasco da Gama</option>
                        </optgroup>

                    </select>

                </Form.Field>
                <Form.Field>
                    <Form.Select
                        placeholder='Escolha a(s) área(s) de interesse'
                        name="interest_areas"
                        label='Áreas de interesse'
                        text={this.state.interest_areas}
                        onChange={this.optChangeAreas}
                        fluid
                        multiple
                        selection
                        required
                        options={interestOptions}
                    />
                </Form.Field>
                <Form.Field>
                    <Form.Select
                        placeholder='Escolha a(s) razão(ões) de voluntariar'
                        label='Razão de voluntariar'
                        name="reason_volunteer"
                        required
                        onChange={this.optChangeReason}
                        fluid
                        multiple
                        selection
                        options={voluntaryOptions}
                    />
                </Form.Field>
                <Form.Field>
                    <Form.Input
                        label='Observações'
                        placeholder='Preciso acesso local'
                        name="observations"
                        value={this.state.observations}
                        onChange={this.onChange}
                    />
                </Form.Field>
                <br></br>
            </div>
            : null;
        return (
            <div class="contain">
                <div class="contain-form-reg">
                    <h1 className="registar">Registar</h1>
                    <Form id="bigform" error={this.state.emailExists} loading={this.state.isLoading} success={this.state.isSuccess} name="reg form" onSubmit={this.onSubmit}>
                        <Form.Field>
                            <Form.Input
                                required
                                label="Nome Completo"
                                placeholder="José Manuel"
                                name="name"
                                value={this.state.name}
                                onChange={this.onChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                required
                                id="formEmail"
                                type="email"
                                label="Email"
                                placeholder="example@example.com"
                                name="email"
                                value={this.state.email}
                                onChange={this.onChange}

                            />
                        </Form.Field>
                        <Form.Field >
                            <Form.Input
                                required
                                type="tel"
                                label="Telemóvel"
                                placeholder="963963963"
                                name="phonenumber"
                                pattern="[0-9]{3}[0-9]{3}[0-9]{3}"
                                value={this.state.phonenumber}
                                onChange={this.onChange}
                            />
                            <small>Formato: 123456789</small>
                        </Form.Field>
                        <FormGroup>
                            <Form.Field>
                                <Form.Input
                                    required
                                    type="password"
                                    label="Password"
                                    placeholder="pass_123ABC"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.onChange}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    required
                                    type="password"
                                    label="Confirmar Password"
                                    placeholder="pass_123ABC"
                                    name="confirmPassword"
                                    value={this.state.confirmPassword}
                                    onChange={this.onChange}
                                />
                            </Form.Field>
                        </FormGroup>
                        <Form.Field>
                            <Form.Checkbox
                                label='Sou membro da Comunidade IPS'
                                name="is_member"
                                value={this.state.is_member}
                                checked={this.state.is_member}
                                onChange={this.onChange && this.toggle}
                            />
                        </Form.Field>
                        <div>
                            {content}
                        </div>
                        <Form.Field >
                            <Form.Checkbox checked={this.state.consent} value={this.state.consent} onChange={this.onChange && this.toggleConsent} required className="autorizar" label='Autorização RGPD' />
                            <p> Consinto, ao abrigo do Regulamento Geral de Proteção de Dados (RGPD),
                            a utilização dos meus dados pessoais, fornecidos no formulário,
                            ficando informado/a do direito a retirar o consentimento a qualquer momento
                            e que o tratamento de dados é da responsabilidade do IPS, sendo-lhe aplicada
                    a Política de Proteção de Dados do IPS (disponível <a href="http://www.si.ips.pt/ips_si/web_base.gera_pagina?P_pagina=40723">aqui</a>).</p>
                        </Form.Field>
                        <Message
                                error
                                header='Email já registado'
                                content='Email já se encontra registado no sistema, por favor insira um diferente ou siga para o login'
                            />
                        <Form.Button type="submit">Registar</Form.Button>
                    </Form>

                </div>
            </div>

        )
    }
}
