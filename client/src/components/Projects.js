import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import DataTable from 'react-data-table-component'
import { Modal, Header, Button, Icon, Form, Radio } from 'semantic-ui-react'
import { getAllProjects, getProject, updateProject, postProject, approveProject, denyProject, applyToProject } from './ProjectFunctions'
import { getUser, getOtherUser } from './UserFunctions'
import { postOtherNotify, readNotification } from './NotificationFunctions'
import './Projects.css'

//filtering 
const FilterComponent = ({ filterText, onFilter, onClear }) => (
    <>
        <input class="search" id="search" type="text" placeholder="Procurar" value={filterText} onChange={onFilter} />
        <button class="clear-btn" type="button" onClick={onClear}>X</button>
    </>
);

export default class Projects extends Component {
    constructor() {
        super()
        this.state = {
            email: '',
            is_member: null,
            consent: true,
            creatingProject: false,
            submitingProject: false,
            value: "false",
            c_project_name: "",
            c_intervention_area: "",
            c_volunteer_areas: "",
            c_summary: "",
            c_inside_project: "",
            c_activity_description: "",
            c_contact_person: "",
            c_target_audience: "",
            c_objectives: "",
            c_org_name: "",
            c_specific_formation: "",
            c_specific_formation_opt: "false",
            c_start_date: "",
            c_end_date: "",
            c_involved_entities: "",
            c_observations: "",
            rejection_notice: "",
            open: false,
            open_reason: false,
            filterText: "",
            resetPaginationToggle: false,
            pending: false,
            projects: [],
            filteredItems: [],
            loading: false,
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    //Handy workaround for target values on change
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }
    optChangeAreas = (e, { value }) => {
        this.state.c_volunteer_areas = value
    }
    //redefines submit method to implement UserFunction/login redirects to profile on ok

    handleChange = (e, { value }) => this.setState({ value })
    toggleConsent = () => {
        this.setState((state) => ({ consent: !state.consent }))
    }
    componentDidMount() {
        if (localStorage.getItem('userToken') === "" || localStorage.getItem('userToken') == null) {
        } else {
            const User = {
                email: this.state.email,
                is_member: this.state.is_member
            }
            getUser(User).then(res => {
                if (!res.error) {
                    this.setState({ email: res.email })
                    this.setState({ is_member: res.is_member })
                }
            })
        }
        this.updateProjectList()
        console.log(this.state.filteredItems)

    }
    closeConfigShow = (closeOnEscape, closeOnDimmerClick) => () => {
        this.setState({ closeOnEscape, closeOnDimmerClick, open: true })
    }

    closeRejectionShow = (closeOnEscape_reason, closeOnDimmerClick_reason, reject_id) => () => {
        this.setState({ reject_id, closeOnEscape_reason, closeOnDimmerClick_reason, open_reason: true })
    }


    updateProjectList() {
        this.setState({ pending: true })
        getAllProjects().then(res => {
            if (!res.error) {
                var joined = []
                this.setState({ projects: joined })
                res.data.result.forEach(element => {

                    getOtherUser(element.project_manager).then(res => {
                        element.project_managerUser = res.data.result
                        if (element.state != "Negado" && element.state != "Arquivado" && element.state != "Por aprovar") {
                            joined.push(element)
                            this.setState({ projects: joined })
                        } else if (element.project_manager == this.state.email || this.state.email == "dnl.afonso1@gmail.com") {
                            joined.push(element)

                            this.setState({ projects: joined })

                        }
                        this.setState({ filteredItems: this.state.projects })
                        this.setState({ pending: false })
                    })


                });



            }
        })
    }

    updateProjectOnList(id) {
        this.setState({ loading: true })
        getProject(id).then(res => {
            if (!res.error) {
                var element = res.data.result

                getOtherUser(element.project_manager).then(res => {
                    element.project_managerUser = res.data.result
                    if (element.state != "Negado" && element.state != "Arquivado" && element.state != "Por aprovar") {
                     
                        var element_found
                        var new_array = []
                        this.state.projects.forEach(elementh => {
                            if (elementh._id == id) {
                                element_found = elementh
                                
                            }else{
                                new_array.push(elementh)
                            }
                        });
                        new_array.push(element)
                        this.setState({ projects: new_array })

                        this.setState({ filteredItems: this.state.projects })


                    } else if (element.project_manager == this.state.email || this.state.email == "dnl.afonso1@gmail.com") {
                        
                        var element_found
                        var new_array = []
                        this.state.projects.forEach(elementh => {
                          
                            if (elementh._id == id) {
                                element_found = elementh
                                
                            }else{
                                new_array.push(elementh)
                            }
                        });
                        
                        new_array.push(element)
                        this.setState({ projects: new_array })
                        this.setState({ filteredItems: this.state.projects })
                    }
                    this.setState({ loading: false })
                })





            }
        })
    }


    onSubmit(e) {
        this.setState({ submitProject: true })
        if (this.state.consent) {
            const Project = {
                project_name: this.state.c_project_name,
                intervention_area: this.state.c_intervention_area,
                volunteer_areas: this.state.c_volunteer_areas,
                summary: this.state.c_summary,
                activity_description: this.state.c_activity_description,
                inside_project: this.state.is_member,
                contact_person: this.state.c_contact_person,
                target_audience: this.state.c_target_audience,
                objectives: this.state.c_objectives,
                org_name: this.state.c_org_name,
                specific_formation: this.state.c_specific_formation,
                start_date: this.state.c_start_date,
                end_date: this.state.c_end_date,
                involved_entities: this.state.c_involved_entities,
                observations: this.state.c_observations
            }
            if (this.state.value === "false") {
                Project.specific_formation = ""
            }
            if (this.state.is_member) {
                Project.inside_project = "Interno"
            } else {
                Project.inside_project = "Externo"
            }
            console.log(Project)
            postProject(Project).then(res => {
                const Notification = {
                    email: "dnl.afonso1@gmail.com",
                    content: res.data.result._id + ": Projeto precisa aprovação",
                    type: "ProjectApproval:" + res.data.result._id
                }
                postOtherNotify(Notification)
                window.navbarComponent.updateNotifications()
                console.log("Projeto submetido :" + res)
                this.setState({ submitProject: false })
                this.close();
                //this.setState({ pending: true })

                this.updateProjectList();
            })
        } else {
            alert("Por favor aceite a autorização RGPD");
            return false;
        }


    }

    close = () => this.setState({ open: false })
    closeReject = () => this.setState({ open_reason: false })

    signUpProject = (id) => {
        applyToProject(id).then(res => {
            this.updateProjectOnList(id)
        }
        )
        console.log('SignedUp');
    }

    handleApproveProject(id) {
        console.log("Approved: " + id)
        approveProject(id).then(res => {
            const profileNotifys = {
                type: "ProjectApproval:" + id
            }
            readNotification(profileNotifys).then(res => {

                window.navbarComponent.updateNotifications()
                this.updateProjectOnList(id)
            })

        }
        )

    }
    handleDenyProject(id) {
        console.log("Denied: " + id)
        denyProject(id, this.state.rejection_notice).then(res => {
            const profileNotifys = {
                type: "ProjectApproval:" + id
            }
            readNotification(profileNotifys).then(res => {

                window.navbarComponent.updateNotifications()

                this.setState({ rejection_notice: "" })
                this.updateProjectOnList(id)
                this.closeReject()
            })

        }
        )
    }
    handleClear = () => {
        const { resetPaginationToggle, filterText } = this.state;

        if (this.state.filterText) {
            this.setState({
                resetPaginationToggle: !resetPaginationToggle
            });
            this.setState({
                filterText: ""
            });
            this.updateProjectList()
        }
    };

    optChangeCourse = (e) => {
        this.state.c_specific_formation = e.target.options[e.target.selectedIndex].text
        console.log("Changed: " + this.state.c_specific_formation + " to " + e.target.options[e.target.selectedIndex].text)
    }


    render() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        const interestOptions = [
            { key: 'aa', text: 'Atividades Académicas', value: 'Atividades Académicas' },
            { key: 'am', text: 'Ambiental', value: 'Ambiental' },
            { key: 'ae', text: 'Apoio a Eventos', value: 'Apoio a Eventos' },
            { key: 'it', text: 'Informática', value: 'Informática' },
            { key: 'c', text: 'Comunicação', value: 'Comunicação' },
            { key: 'ct', text: 'Cultural', value: 'Cultural' },
            { key: 'de', text: 'Desporto', value: 'Desporto' },
            { key: 'ed', text: 'Educação', value: 'Educação' },
            { key: 'sa', text: 'Saúde', value: 'Saúde' },
            { key: 'so', text: 'Social', value: 'Social' }

        ]
        const { open, closeOnEscape, closeOnDimmerClick } = this.state;
        const { open_reason, closeOnEscape_reason, closeOnDimmerClick_reason, reject_id } = this.state;

        const specificRequired = (this.state.value === "true")

            ? <Form.Field>
                <label>Curso/Formação</label>
                <select name="c_specific_formation" onChange={this.optChangeCourse}>
                    <option value="" selected disabled hidden>Escolha aqui</option>
                    <optgroup label="Formação">
                        <option value="34">Não Definido</option>
                        <option value="35">Ensino Básico (9º Ano)</option>
                        <option value="36">Ensino Secundário - Incompleto</option>
                        <option value="37">Ensino Secundário Completo</option>
                        <option value="38">Curso Tecnico-Profissional (Equivalência Ensino Secundário)</option>
                        <option value="39">Formação</option>
                        <option value="40">Propdêutico</option>
                        <option value="41">Bacharelato</option>
                        <option value="42">Licenciatura</option>
                        <option value="43">Pós-Graduação</option>
                        <option value="44">Mestrado</option>
                        <option value="45">Doutoramento</option>
                        <option value="46">CET</option>
                        <option value="47">MBA</option>
                        <option value="48">CTeSP</option>
                        <option value="49">Licenciatura - Incompleto</option>
                        <option value="50">Mestrado - Incompleto</option>
                    </optgroup>
                </select>

            </Form.Field> : null;
        const isExternal = this.state.is_member ? null : <Form.Field>
            <Form.Input
                label="Nome da organização"
                placeholder='Santander'
                name="c_org_name"
                required
                value={this.state.c_org_name}
                onChange={this.onChange} />
        </Form.Field>;

        const endDateInput = (this.state.c_start_date != "") ?
            <Form.Field>
                <Form.Input
                    label="Data de fim"
                    type="date"
                    name="c_end_date"
                    min={this.state.c_start_date}
                    required
                    value={this.state.c_end_date}
                    onChange={this.onChange}
                />
            </Form.Field> : null;
        const modal =
            <Modal
                open={open}
                closeOnEscape={closeOnEscape}
                closeOnDimmerClick={closeOnDimmerClick}
                onClose={this.close}
            >
                <Modal.Header>Proposta de projeto</Modal.Header>
                <Modal.Content scrolling>
                    <Form loading={this.state.submitingProject} onSubmit={this.onSubmit}>
                        {isExternal}
                        <Form.Field>
                            <Form.Input
                                label="Nome do projeto"
                                placeholder='Campanha de Solidariedade'
                                name="c_project_name"
                                required
                                value={this.state.c_project_name}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Form.Field>
                            <Form.TextArea
                                label="Resumo do projeto"
                                placeholder='Campanha de Solidariedade para ajudar os mais carênciados em...'
                                name="c_summary"
                                required
                                value={this.state.c_summary}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                label="Área de intervenção"
                                placeholder='Formação'
                                name="c_intervention_area"
                                required
                                value={this.state.c_intervention_area}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                label="Público alvo (Beneficários)"
                                placeholder='Estudantes de INF'
                                name="c_target_audience"
                                required
                                value={this.state.c_target_audience}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                label="Objetivos"
                                placeholder='Ajudar os beneficiarios...'
                                name="c_objectives"
                                value={this.state.c_objectives}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                label="Descrição das Atividades"
                                placeholder=''
                                name="c_activity_description"
                                required
                                value={this.state.c_activity_description}
                                onChange={this.onChange} />
                        </Form.Field>

                        <Form.Field >
                            Exigência de formação específica:
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Não'
                                name='radioGroup'
                                value='false'
                                checked={this.state.value === "false"}
                                onChange={this.handleChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Sim'
                                name='radioGroup'
                                value='true'
                                checked={this.state.value === "true"}
                                onChange={this.handleChange}
                            />
                        </Form.Field>
                        {specificRequired}
                        <Form.Field>
                            <Form.Input
                                label="Data de inicío"
                                type="date"
                                name="c_start_date"
                                required
                                min={today}
                                max={this.state.c_end_date}
                                value={this.state.c_start_date}
                                onChange={this.onChange}
                            />
                        </Form.Field>
                        {endDateInput}
                        <Form.Field>
                            <Form.Select
                                placeholder='Escolha a(s) área(s) de interesse'
                                name="c_volunteer_areas"
                                label='Para a concretização do Projeto/Atividades, em que áreas necessita de voluntários'
                                text={this.state.c_volunteer_areas}
                                onChange={this.optChangeAreas}
                                fluid
                                multiple
                                selection
                                required
                                options={interestOptions}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                label="Entidades envolvidas"
                                placeholder='IPS'
                                name="c_involved_entities"
                                value={this.state.c_involved_entities}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                label="Observações"
                                placeholder='Parece interessante'
                                name="c_observations"
                                value={this.state.c_observations}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Button type="submit"
                            labelPosition='right'
                            positive
                            icon='checkmark'
                            content='Propor Projeto'
                        />
                        <br></br>
                    </Form>
                </Modal.Content>
                <Modal.Actions>

                    <Button onClick={this.close} negative>
                        Cancelar
                </Button>

                </Modal.Actions>
            </Modal>;

        const modalReject =
            <Modal
                open={open_reason}
                closeOnEscape={closeOnEscape_reason}
                closeOnDimmerClick={closeOnDimmerClick_reason}
                reject_id={reject_id}
                onClose={this.closeReject}
            >
                <Modal.Header>Negação de projeto</Modal.Header>
                <Modal.Content scrolling>
                    <Form onSubmit={() => this.handleDenyProject(reject_id)}>

                        <Form.Field>
                            <Form.Input
                                label="Razão de negação"
                                placeholder='Falta de...'
                                required
                                name="rejection_notice"
                                value={this.state.rejection_notice}
                                onChange={this.onChange} />
                        </Form.Field>
                        <Button type="submit"
                            labelPosition='right'
                            positive
                            icon='checkmark'
                            content='Submeter rejeição'
                        />
                        <br></br>
                    </Form>
                </Modal.Content>
                <Modal.Actions>

                    <Button onClick={this.closeReject} negative>
                        Cancelar
                </Button>

                </Modal.Actions>
            </Modal>;

        //Table data

        const columns = [
            {
                right: true,
                cell: row => {
                    var notify
                    if (window.navbarComponent.projectHasNotification(row._id)) {
                        notify = <div className="dot"></div>
                    }
                    return (notify)
                },
                width: "5%"
            },
            {
                name: 'Nome do Projeto',
                selector: 'project_name',
                sortable: true,

            },
            {
                name: 'Nome do Gestor',
                cell: row => {
                    return (<div ><Link to={'/otherUser/' + row.project_managerUser.email}> {row.project_managerUser.name}</Link></div>)
                }
            },
            {
                name: 'Estado',
                selector: 'state',
                sortable: true,
                maxWidth: "50px",
            },
            {
                name: 'Tipo',
                selector: 'inside_project',
                sortable: true,
                maxWidth: "50px",
            },
            {
                name: 'Área de Interesse',
                selector: 'volunteer_areas',
                sortable: true,
            },
            {
                name: 'Inscrever',
                button: true,
                ignoreRowClick: true,
                maxWidth: "20%",
                cell: row => {
                    if (row.state === "Por aprovar") {
                        return (<div>Fechado</div>)
                    } else if (row.state === "Aprovado") {
                        if (row.candidates != null) {
                            var approved = [];
                            var denied = [];
                            if (row.approved_candidates != null)
                                approved.push(row.approved_candidates)
                            if (row.denied_candidates != null)
                                denied.push(row.denied_candidates)
                            if (row.candidates.includes(this.state.email)) {
                                return (<div>Submetido</div>)
                            } else if (approved.includes(this.state.email)) {
                                return (<div>Inscrito</div>)
                            } else if (denied.includes(this.state.email)) {
                                return (<div>Negado</div>)
                            } else {
                                return (
                                    <div>
                                        <button type="button" class="inscrever" onClick={() => this.signUpProject(row._id)}>Inscrever</button>

                                    </div>
                                )
                            }
                        } else {
                            return (
                                <div>
                                    <button type="button" class="inscrever" onClick={() => this.signUpProject(row._id)}>Inscrever</button>

                                </div>
                            )
                        }

                    }
                },

            },


        ]
        //Pagination
        const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };
        //Onclick details 

        //Conditional for Comissão to add Accept or deny button
        if (this.state.email === "dnl.afonso1@gmail.com") {
            columns.push({
                name: 'Aprovar Projeto',
                button: true,
                ignoreRowClick: true,
                width: "20%",
                cell: row => {
                    if (row.state === "Por aprovar") {
                        return (
                            <div >
                                <button type="button" class="proj-aceitar" onClick={() => this.handleApproveProject(row._id)}>Aceitar</button>
                                <br />
                                <button type="button" class="proj-recusar" onClick={this.closeRejectionShow(true, false, row._id)}>Recusar</button>
                            </div>
                        )
                    } else {
                        return (
                            <div>
                                {row.state}
                            </div>
                        )
                    }
                }
                ,

            })
        }


        return (
            <div>
                {modalReject}
                {modal}

                <div id="containerC-div-id" class="container-div" >

                    <div class="options-div">
                        <FilterComponent
                            onFilter={(e) => {
                                let newFilterText = e.target.value;
                                this.state.filteredItems = this.state.projects.filter(
                                    (item) =>
                                        item.project_name &&
                                        item.project_name.toLowerCase().includes(newFilterText.toLowerCase())
                                );
                                this.setState({ filterText: newFilterText });
                            }}
                            onClear={this.handleClear}
                            filterText={this.state.filterText}
                        />



                        <button class="propor" disabled={(this.state.email == "")} onClick={this.closeConfigShow(true, false)}>Propor Projeto</button>

                    </div>
                  <Form loading={this.state.loading}>
                         <DataTable
                        title="Lista de Projetos"
                        persistTableHead
                        columns={columns}
                        data={this.state.filteredItems}
                        defaultSortField="project_name"
                        fixedHeader
                        fixedHeaderScrollHeight="300px"
                        highlightOnHover
                        pagination
                        noDataComponent="Nenhum projeto foi encontrado..."
                        fixedHeaderScrollHeight
                        progressPending={this.state.pending}
                        paginationComponentOptions={paginationOptions}
                        paginationResetDefaultPage={this.state.resetPaginationToggle}
                        onRowClicked={row => {
                            this.props.history.push('/projectsdetails/' + row._id)
                        }}

                    /> 
                    </Form>
                        
                </div>
            </div>
        )
    }
}
