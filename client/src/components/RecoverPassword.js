import React, { Component } from 'react'
import './RecoverPassword.css'
import { recover } from './UserFunctions'
import { Form,Message } from 'semantic-ui-react'

//Login Page TODO make it pretty
class RecoverPassword extends Component {
    constructor() {
        super()
        this.state = {
            email: '',
            emailError:false,
            isLoading:false
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    //Handy workaround for target values on change
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }
    //redefines submit method to implement UserFunction/login redirects to profile on ok
    onSubmit(e) {
        e.preventDefault()
        this.setState({emailError:false})
        this.setState({isLoading:true})
        const recoverMail = {
            email: this.state.email
        }
      
        recover(recoverMail).then(response => {
            if (response) {
                alert("Email enviado!")
              
                this.props.history.push('/login')

            }else{
                this.setState({emailError:true})
            }
            this.setState({isLoading:false})
        })
    }

    render() {
        return (
            <div className="contain">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <Form error={this.state.emailError} loading={this.state.isLoading} onSubmit={this.onSubmit}>
                            <h1 className="h3 mb-3 font-weight-normal">Recupere a sua password</h1>
                            
                            <div className="form-group">
                            <Message
                                error
                                header='Email Inválido ou inexistente'
                                content='Por favor insira um email válido e registado'
                            />
                                <label htmlFor="email"> Email </label>
                                <input type="email"
                                    className="form-control"
                                    name="email"
                                    placeholder="example@example.example"
                                    value={this.state.email}
                                    onChange={this.onChange} />

                            </div>
                           
                            <button type="submit"
                                className="btn btn-lg btn-primary btn-block">
                                Enviar email
                            </button>
                        </Form>
                    
                    </div>
                </div>
            </div>
        )
    }
}

export default RecoverPassword