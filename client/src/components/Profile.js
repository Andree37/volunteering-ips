import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Message } from 'semantic-ui-react'
import axios from 'axios'
import './Profile.css'
import { getUser, getOtherUser } from './UserFunctions'
import { getAllProjects } from './ProjectFunctions'

import DataTable from 'react-data-table-component'

class Profile extends Component {
    constructor() {
        super()
        this.state = {
            name: "",
            email: "",
            phonenumber: "",
            password: "",
            confirmPassword: "",
            is_member: false,
            type_member: "",
            birthdate: "",
            school: "",
            course: "",
            interest_areas: "",
            reason_volunteer: "",
            observations: "",
            profile_picture: "",
            resetPaginationToggle: false,
            projects: [],
            terminated_projects:[],
        }
    }


    componentDidMount() {
        if (localStorage.getItem('userToken') === "" || localStorage.getItem('userToken') == null) {
            this.props.history.push('/login')
        } else {
            const User = {
                name: this.state.name,
                email: this.state.email,
                phonenumber: this.state.phonenumber,
                password: this.state.password,
                is_member: this.state.is_member,
                type_member: this.state.type_member,
                birthdate: this.state.birthdate,
                school: this.state.school,
                course: this.state.course,
                interest_areas: this.state.interest_areas,
                reason_volunteer: this.state.reason_volunteer,
                observations: this.state.observations,
                profile_picture: this.state.profile_picture
            }

            getUser(User).then(res => {
                if (!res.error) {
                    console.log(res)
                    this.setState({ name: res.name })
                    this.setState({ email: res.email })
                    this.setState({ phonenumber: res.phonenumber })

                } else {
                    console.log(res.error)
                }
            }).then(
                axios.get('/users/picture', { responseType: 'blob' })
                    .then(res => {
                        console.log(res.data)
                        this.setState({ profile_picture: URL.createObjectURL(res.data) });
                        getAllProjects().then(res => {
                            if (!res.error) {
                                var joined = []
                                var terminated = []
                                this.setState({ projects: joined })
                                this.setState({ terminated_projects: terminated })
                                res.data.result.forEach(element => {
                                    if (element.approved_candidates != null) {
                                        element.approved_candidates.forEach(candidate => {
                                            if (candidate == this.state.email) {
                                                getOtherUser(element.project_manager).then(res => {
                                                    element.project_managerUser = res.data.result
                                                    
                                                   
                                                    if(element.state == "Aprovado"){
                                                        joined.push(element)
                                                        this.setState({ projects: joined })
                                                    }else if(element.state == "Terminado" || element.state == "Arquivado" ){
                                                        terminated.push(element)
                                                        this.setState({ terminated_projects: terminated })
                                                    }
                                                    
                                                })
                                            }
                                        })
                                    }
                                });
                                console.log(this.state.projects)

                            }
                        })
                    }))
        }
    }



    render() {
        const newProfileNote = (localStorage.getItem('ProfileNotifications') === 'Profile') ?

            <Message

                header='Nova Conta?'
                content='Atualize a sua foto de Perfil no editar!'
            />

            : null
        //Table data

        const columns = [
            {
                name: 'Nome do Projeto',
                selector: 'project_name',
                sortable: true,
            },
            {
                name: 'Nome do Gestor',
                sortable: true,
                cell: row => {
                return (<div ><Link to={'/otherUser/' + row.project_managerUser.email}> {row.project_managerUser.name}</Link></div>)
                }
            },
            {
                name: 'Estado',
                selector: 'state',
                sortable: true,
                maxWidth: "50px",
            },
            {
                name: 'Tipo',
                selector: 'inside_project',
                sortable: true,
                maxWidth: "50px",
            },
            {
                name: 'Área de Interesse',
                selector: 'volunteer_areas',
                sortable: true,
            },
            
            


        ]
        //Pagination
        const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };
       
        return (
            <div class="contain">
                <div class="info-perfil-top notify">{newProfileNote}</div>

                <div class="contain-form">
                    <div class="info">
                        <img class="img-perfil" alt="Imagem de perfil" src={this.state.profile_picture} />

                        <label class="info-perfil nome">Nome Completo : {this.state.name}</label>
                        <label class="info-perfil email">Email : {this.state.email}</label>
                        <label class="info-perfil tele">Telemóvel : {this.state.phonenumber}</label>
                        <div class="button-edit">
                            <Link class="info-perfil link" to='/EditPerfil'>
                                <button class="info-perfil editar">Editar</button>
                            </Link>
                        </div>
                        <div class="break"></div>

                        <div class="projetos-a-decorrer">
                            <h1>Projetos a Decorrer</h1>
                            <DataTable
                                columns={columns}
                                data={this.state.projects}
                                defaultSortField="project_name"
                                noHeader
                                fixedHeader
                                fixedHeaderScrollHeight="100px"
                                highlightOnHover
                                noDataComponent="Nenhum projeto do qual participa etá a decorrer..."
                                persistTableHead

                                pagination
                                paginationComponentOptions={paginationOptions}
                                paginationResetDefaultPage={this.state.resetPaginationToggle}
                                onRowClicked={row => {
                                    this.props.history.push('/projectsdetails/' + row._id)
                                }}

                            />
                        </div>
                        <div class="projetos-terminados">
                            <h1 class="proj-term-h1">Projetos Terminados</h1>
                            <DataTable class="proj-term-dt"
                                columns={columns}
                                data={this.state.terminated_projects}
                                defaultSortField="project_name"
                                noHeader
                                fixedHeader
                                fixedHeaderScrollHeight="100px"
                                highlightOnHover
                                noDataComponent="Nenhum projeto do qual participou foi terminado..."
                                persistTableHead

                                pagination
                                paginationComponentOptions={paginationOptions}
                                paginationResetDefaultPage={this.state.resetPaginationToggle}
                                onRowClicked={row => {
                                    this.props.history.push('/projectsdetails/' + row._id)
                                }}

                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Profile