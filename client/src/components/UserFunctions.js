
import axios from 'axios'

//This file is to translate API calls to functions
// pod "http://pvipsback-service.pvips-dev.svc.cluster.local/"
//local "http://localhost:8000"
//"proxy": "http://35.189.210.245:80/",

//Register a user
export function register(newUser){
    return axios
    .post('users/register/',{
            name: newUser.name,
            email: newUser.email,
            phonenumber: newUser.phonenumber,
            password: newUser.password,
            is_member: newUser.is_member,
            type_member: newUser.type_member,
            birthdate: newUser.birthdate,
            school: newUser.school,
            course: newUser.course,
            interest_areas: newUser.interest_areas,
            reason_volunteer: newUser.reason_volunteer,
            observations: newUser.observations
    })
    .then(res=>{
        console.log(res)
        return res
    })
    .catch(err => {
        console.log("Register user had a little problem: "+err)
    })
}


export function getUser(User){
    return axios
    .get('users/',{
            name: User.name,
            email: User.email,
            phonenumber: User.phonenumber,
            is_member: User.is_member,
            type_member: User.type_member,
            birthdate: User.birthdate,
            school: User.school,
            course: User.course,
            interest_areas: User.interest_areas,
            reason_volunteer: User.reason_volunteer,
            observations: User.observations,
            profile_picture: User.profile_picture
    })
    .then(res => {
        localStorage.setItem('user', res.data.result)
        return res.data.result
     })
     .catch(err => {
         console.log("Get user had a little problem: "+err)
     })
}
export function getOtherUser(email){
    return axios
    .get('users/'+ email)
}

export function updateUserPWD(updatedUser){
    return axios
    .put('users/',{
            name: updatedUser.name,
            email: updatedUser.email,
            phonenumber: updatedUser.phonenumber,
            password: updatedUser.password,
            is_member: updatedUser.is_member,
            type_member: updatedUser.type_member,
            birthdate: updatedUser.birthdate,
            school: updatedUser.school,
            course: updatedUser.course,
            interest_areas: updatedUser.interest_areas,
            reason_volunteer: updatedUser.reason_volunteer,
            observations: updatedUser.observations,
            profile_picture: updatedUser.profile_picture
    })
    .then(res=>{
        console.log(res)
        return res
    })
    .catch(err => {
        console.log("Update user had a little problem: "+err)
    })
}
export function updateUser(updatedUser){
    return axios
    .put('users/',{
            name: updatedUser.name,
            email: updatedUser.email,
            phonenumber: updatedUser.phonenumber,
            is_member: updatedUser.is_member,
            type_member: updatedUser.type_member,
            birthdate: updatedUser.birthdate,
            school: updatedUser.school,
            course: updatedUser.course,
            interest_areas: updatedUser.interest_areas,
            reason_volunteer: updatedUser.reason_volunteer,
            observations: updatedUser.observations,
            profile_picture: updatedUser.profile_picture
    })
    .then(res=>{
        console.log(res)
        return res
    })
    .catch(err => {
        console.log("Update user had a little problem: "+err)
    })
}

//Logs a user
export function login(userLog) {
    return axios
    .post('users/login/',{
        email:userLog.email,
        password:userLog.password
    })
    
}
//Recovers a users password
export function recover(recoverMail) {
    return axios
    .post('/users/recover_password/',{
        email:recoverMail.email
    })
    .then(response => {
        if(response.data.error==null){
            console.log(response.data.token)
            return response
            }else{
                return false
            }
    })
    .catch(err => {
        console.log("Recover password had a little problem: "+err)
        return err
    })
}
export function resetUserPassword(updatedUser){
    var token = localStorage.getItem('resetToken').toString()
    console.log(token)
    return axios
    .put('users/recover_password/'+token.toString()+'/',{          
        new_password: updatedUser.password
    })
    .then(res=>{
        console.log(res)
        return res
    })
    .catch(err => {
        console.log("Update user had a little problem: "+err)
    })
}

export function logout() {
    return axios
    .get('users/logout/',{
    })
    .then(res=>{
        console.log(res)
        return res
    }).catch(err => {
        console.log("Logout had a little problem: "+err)
    })
}



export function authorize(recoverMail) {
    var token = localStorage.getItem('authToken').toString()
    console.log(token)
    return axios
    .post('/users/login_token/'+token.toString())

}
export function sendAuthMail(recoverMail) {
    return axios
    .post('/users/resend_auth',{
        email:recoverMail.email
    })
}