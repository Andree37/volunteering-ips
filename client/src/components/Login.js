import React, { Component } from 'react'
import './Login.css'
import { Link } from 'react-router-dom'
import { login } from './UserFunctions'
import { Form, Message, Icon } from 'semantic-ui-react'


//Login Page TODO make it pretty
class Login extends Component {

    constructor() {
        super()
        this.state = {
            email: '',
            password: '',
            invalidation: false,
            isLoading: false,
            authed:false
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    //Handy workaround for target values on change
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }
    //redefines submit method to implement UserFunction/login redirects to profile on ok
    onSubmit(e) {
        e.preventDefault()
        this.setState({ authed: false })
        this.setState({ invalidation: false })
        const userLog = {
            email: this.state.email,
            password: this.state.password
        }
        this.setState({ isLoading: true })
        login(userLog).then(response => {
            this.setState({ isLoading: false })
            if(response.data.token){
                localStorage.setItem('userToken', response.data.token)
                this.props.history.push('/')
                window.location.reload()
            }else if(response.data.error && response.data.error.toString().includes("AuthError")){
                localStorage.setItem('email', this.state.email)
                this.setState({ authed: true })
            }else{
                this.setState({ invalidation: true })
            }
        })
        .catch(error => {
            console.log("Login had a little problem: "+error)
            
        })
    }


    render() {

        return (
                        <Form className="log-form" warning={this.state.authed}error={this.state.invalidation} loading={this.state.isLoading} onSubmit={this.onSubmit}>
                
                <h1 className="title-login">Login</h1>
                
                            <Message
                                error
                                header='Login Inválido'
                                content='Email e/ou Palavra Chave inválida'
                            />
                            <Message warning>
                                    <Message.Header>Email não foi autorizado</Message.Header>
                                    <p>Email não autorizado, visite o seu email ou abra o link e siga as instruções: </p>
                                     <Message.List>
                                         <Message.Item>
                                            <Link to="/authorization">Precisa ajuda a autenticar?</Link>
                                         </Message.Item>
                                     </Message.List>
                                </Message>
                               
        
                <div className="div-email">
                    <label clas="lab-email" htmlFor="email"> Email </label>
                    <input type="email"
                        className="email-input"
                        name="email"
                        required
                        placeholder="example@example.example"
                        value={this.state.email}
                        onChange={this.onChange} />
                            
                </div>
                <div className="div-pass">
                    <label class="lab-pass" htmlFor="password"> Password </label>
                    <input type="password"
                        className="pass-input"
                        name="password"
                        required
                        placeholder="pass_ABC123"
                        value={this.state.password}
                        onChange={this.onChange} />
                        
                    <Link class="esq" to="/recover"> Esqueceu-se da password? </Link>
                    <button type="submit" className="btn-signin">Login</button>
                    </div>
                    <Message class="bottom-link" attached='bottom' >
                                <Icon name='help' />Ainda não se registou?&nbsp;<Link to="/register">Registe-se aqui</Link>&nbsp;.
                            </Message>
                    
                   
                    
            </Form>
            
        )
    }
}

export default Login