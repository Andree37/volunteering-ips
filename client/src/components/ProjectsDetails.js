import React, { Component } from 'react'
import { getProject, updateProject, archiveProject, closeProject, approveCandidate, denyCandidate, postReview, postQuitter,postManyQuitters, denyManyCandidate, approveManyCandidate } from './ProjectFunctions'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { Form, FormGroup, Image, Button, Message, Rating, Input, Label, Modal } from 'semantic-ui-react'
import DataTable from 'react-data-table-component'

import jsPDF from 'jspdf'
import  "jspdf-autotable";
import differenceBy from 'lodash/differenceBy';
import { withRouter } from 'react-router';
import './ProjectsDetails.css'
import './ProjectsInscritos.css'
import { getOtherUser, getUser } from './UserFunctions';

const FilterComponent = ({ filterText, onFilter, onClear }) => (
    <>
        <button type="button" onClick={onClear}>X</button><input class="search" id="search" type="text" placeholder="Procurar" value={filterText} onChange={onFilter} />

    </>
);
const SampleExpandedComponent = ({ data }) => (

    <p>
        Comentário: {data.review}
    </p>
);

class ProjectsDetails extends Component {
    constructor() {
        super()
        this.state = {
            project: '',
            user: '',
            email: '',
            is_member: '',
            filterText: "",
            resetPaginationToggle: false,
            candidates: [],
            isLoading: false,
            isInvalid: false,
            pending: false,
            termindated: false,
            global_rating: 0,
            hasMadeReview: false,
            hasParticipated: false,
            userReview: '',
            userRating: 0,
            participants: [],
            quitters: []
        }
        this.filteredCandidatesItems = []
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)


    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        this.setState({ isLoading: true })
        if (e == "close") {
            var emails = []

            this.state.quitters.forEach(element => {
                emails.push(element.email)
            })
            postManyQuitters(this.state.project._id, emails).then(res => {
                closeProject(this.state.project._id).then(res => {
                    if (!res.error) {
                        this.exportPDF(this.state.participants)
                        this.setState({ isLoading: false })
                        window.location.reload()
                    }
                })
            })





        } else if (e == "archive") {
            archiveProject(this.state.project._id).then(res => {
                if (!res.error) {
                    this.setState({ isLoading: false })
                    window.location.reload()
                }
            })
        }

    }
    componentDidMount() {


        if (localStorage.getItem('userToken') === "" || localStorage.getItem('userToken') == null) {
        } else {
            const User = {
                email: this.state.email,
                is_member: this.state.is_member
            }
            
         
            getUser(User).then(res => {
                if (!res.error) {
                    this.setState({ email: res.email })
                    this.setState({ is_member: res.is_member })
                }
            })
        }
        this.updateCandidateList();

        console.log("Filtered")
        console.log(this.filteredCandidatesItems)
    }

    updateCandidateList() {
        var project = null
        var joinedC = []
        this.setState({ candidates: joinedC })
        this.filteredCandidatesItems = []

        getProject(localStorage.getItem('projectid')).then(res => {
            project = res.data.result
            this.setState({ project: res.data.result })
            if (project.state == "Terminado" || project.state == "Arquivado") {
                this.setState({ termindated: true })
                var total_rating = 0;
                var global = 0;
                if (project.reviews != null) {
                    project.reviews.forEach(review => {
                        total_rating += review.rating;
                        if (review.email == this.state.email) {

                            this.setState({ hasMadeReview: true })
                            this.setState({ userRating: review.rating })
                            this.setState({ userReview: review.review })
                        }
                        getOtherUser(review.email).then(res => {
                            review.name = res.data.result.name
                        }
                        )

                    });
                    if (project.reviews.length > 0) {
                        global = (total_rating / project.reviews.length)
                    } else {
                        global = 0
                    }

                    this.setState({ reviews: project.reviews })
                    console.log(project.reviews)
                    this.setState({ global_rating: global })
                }


            }
            getOtherUser(res.data.result.project_manager).then(res => {
                this.setState({ user: res.data.result })
                var participants = []

                if (project.candidates != null) {
                    project.candidates.forEach(element => {
                        getOtherUser(element).then(res => {

                            var candidate = res.data.result
                            var candidate_status = "Candidato"
                            candidate.candidate_status = candidate_status
                           

                            if (project.denied_candidates != null) {
                                project.denied_candidates.forEach(elementdeny => {
                                    if (elementdeny == element) {
                                        candidate.candidate_status = "Rejeitado"

                                    }
                                });
                            }

                            if (project.approved_candidates != null) {
                                project.approved_candidates.forEach(elementapp => {
                                    if (elementapp == element) {
                                        candidate.candidate_status = "Aceite"
                                        participants.push(candidate)
                                        if (candidate.email == this.state.email) {
                                            this.setState({ hasParticipated: true })
            
                                        }
                                    }
                                  
                                    
                                });
                            }
                            if (project.quit_candidates != null) {
                                project.quit_candidates.forEach(elementq => {
                                    if (elementq == element) {
                                        candidate.candidate_status = "Desistiu"
                                    }
                                });
                            }
                            joinedC.push(candidate)
                            this.filteredCandidatesItems.push(candidate);
                            this.setState({ pending: false })
                            this.setState({ participants: participants })
                            this.setState({ candidates: joinedC })
                        })

                    });



                }

            })
            console.log(this.state.project)



        })

    }

    handleApproveCandidate(id, email) {
        console.log("Approved: " + id)
        this.setState({ pending: true })
        approveCandidate(id, email).then(res => {
            this.setState({ pending: false })
            this.updateCandidateList()
        }
        )

    }
    handleDenyCandidate(id, email) {
        console.log("Denied: " + id)
        this.setState({ pending: true })
        denyCandidate(id, email).then(res => {
            this.setState({ pending: false })
            this.updateCandidateList()
        }
        )
    }

    handleRate = (e, { rating, maxRating }) =>
        this.setState({ userRating: rating, maxRating })

    submitReview() {
        var id = this.state.project._id;
        var rating = this.state.userRating
        var review = this.state.userReview
        postReview(id, rating, review).then(res => {
            console.log(res)
            this.updateCandidateList()
        })
    }

    closeConfigShow = (closeOnEscape, closeOnDimmerClick) => () => {
        this.setState({ closeOnEscape, closeOnDimmerClick, open: true })
    }
    close = () => this.setState({ open: false })

    handleCloseProject() {
        this.onSubmit("close")
    }
    exportPDF = (participants) => {
        const unit = "pt";
        const size = "A4"; // Use A1, A2, A3 or A4
        const orientation = "portrait"; // portrait or landscape
    
        const marginLeft = 40;
        const doc = new jsPDF(orientation, unit, size);
    
        doc.setFontSize(15);
    
        const title = this.state.project.project_name;
        const headers = [["Nome", "Email","Telemóvel", "Formação/Escola"]];
    
        const data = participants.map(elt=> [elt.name, elt.email, elt.phonenumber, elt.school]);
    
        let content = {
          startY: 50,
          head: headers,
          body: data
        };
    
        doc.text(title,marginLeft, 40);
        doc.autoTable(content);
        doc.save("Relatório de "+this.state.project.project_name+".pdf")
      }

    render() {
        localStorage.setItem('projectid', this.props.match.params.id)
        const rejectNote = (this.state.project.state == "Negado" || this.state.project.rejection_notice != null) ?

            <Message
                error
                header='Projeto Chumbado'
                content={"O projeto foi negado porque: " + this.state.project.rejection_notice}
            />

            : null;

        const closedOptionsNote = (this.state.project.project_manager == this.state.email) ?

            <Message.List>
                <Message.Item>
                    <p>Arquivar o projeto</p>
                </Message.Item>
                <Message.Item>
                    <p>Avaliar o projeto e deixar um comentário</p>
                </Message.Item>
                <Message.Item>
                    <p>Descarregar a lista de participantes para pdf</p>
                </Message.Item>
            </Message.List>



            : <Message.List>
                <Message.Item>
                    <p>Se participou no projeto pode avaliar o projeto e deixar um comentário</p>
                </Message.Item>
            </Message.List>;

        const approvedOptionsNote = (this.state.project.project_manager == this.state.email) ?

            <Message.List>
                <Message.Item>
                    <p>Aprovar candidatos</p>
                </Message.Item>
                <Message.Item>
                    <p>Fechar o projeto</p>
                </Message.Item>
            </Message.List>



            : <Message.List>
                <Message.Item>
                    <p>Participar no projeto, e aguardar para ser aprovado pelo gestor do projeto</p>
                </Message.Item>
            </Message.List>;

        const closedNote = (this.state.project.state == "Terminado") ?

            <Message warning>
                <Message.Header>Projeto terminou</Message.Header>
                <p>Este projeto terminou, as ações que pode tomar:</p>
                <Message.List>
                    {closedOptionsNote}
                </Message.List>
            </Message>

            : null;
        const awaitAprovalNote = (this.state.project.state == "Por aprovar") ?

            <Message warning>
                <Message.Header>Projeto a aguardar aprovação</Message.Header>
                <p>Este projeto está por ser aprovado pela comissão, aguarde pela aprovação</p>
            </Message>

            : null;
        const approvedNote = (this.state.project.state == "Aprovado") ?

            <Message success>
                <Message.Header>Projeto foi aprovado!</Message.Header>
                <p>Este projeto foi aprovado pela comissão, as ações que pode tomar:</p>
                <Message.List>
                    {approvedOptionsNote}
                </Message.List>
            </Message>

            : null;
        const reviews = (this.state.project.state == "Terminado" || this.state.project.state == "Arquivado") ?

            <Tab>Comentários</Tab>

            : null;
        const review_submit = (!this.state.hasMadeReview && this.state.hasParticipated && this.state.project.state != "Arquivado") ?
            <Form onSubmit={this.submitReview.bind(this)}>
                <Form.Field required>
                    <Label>Rating</Label><Rating icon='star' maxRating={5} onRate={this.handleRate} />
                </Form.Field>
                <Form.Field>
                    <Input required label="Comentário" placeholder="Foi giro..." name="userReview" value={this.state.userReview} onChange={this.onChange}>
                    </Input>
                </Form.Field>
                <Button className="subm-feed" type="submit">Submeter feedback</Button>
            </Form> :
            <Form>
                <Form.Field >
                    <Label>Rating</Label><Rating icon='star' defaultRating={this.state.userRating} maxRating={5} disabled />
                </Form.Field>
                <Form.Field>
                    <Label>Comentário:</Label><text>{this.state.userReview}</text>
                </Form.Field>
            </Form>
            ;
        const review_columns = [
            {
                name: 'Nome do participante',
                selector: 'name',
                sortable: true,
                right: false,
            },
            {
                name: 'Rating',
                selector: 'rating',
                right: false,
                sortable: true,
                cell: row => {
                    return (<div ><Rating icon='star' defaultRating={row.rating} maxRating={5} disabled /></div>)
                }
            },
            {
                name: 'Comentário',
                selector: 'review',
                right: false,
            },

        ]


        const detailsOrgNameLabel = (this.state.project.inside_project === "Externo") ?
            <label>Nome da Organização</label>
            : null;
        const detailsOrgNameText = (this.state.project.inside_project === "Externo") ?
            <text>{this.state.project.org_name}</text>
            : null;

        //Table data

        const columns = [
            {
                name: 'Nome do Candidato',
                selector: 'name',
                sortable: true,
                right: false,
            },
            {
                name: 'Tipo de Membro',
                selector: 'type_member',
                right: false,
                sortable: true,
            },
            {
                name: 'Escola/Serviço',
                selector: 'school',
                sortable: true,
                right: false,
            },
            {
                name: 'Estado do Candidato',
                selector: 'candidate_status',
                left: true,
                sortable: true,
            },


        ]
        //Pagination
        const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };
        //Onclick details 


        const SelectableCandidatesManagement = () => {

            const [filterText, setFilterText] = React.useState('');
            const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
            const filteredItems = this.state.candidates.filter(item => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()));
            const [selectedRowsC, setSelectedRowsC] = React.useState([]);
            const [toggleClearedC, setToggleClearedC] = React.useState(false);
            const [dataC, setDataC] = React.useState(filteredItems);
            const rowDisabledCriteria = row => { return row.candidate_status != "Candidato" };

            const handleRowSelectedC = React.useCallback(state => {
                setSelectedRowsC(state.selectedRows);
            }, []);

            const contextActionsC = React.useMemo(() => {
                const handleAccept = () => {

                    if (window.confirm(`Tem a certeza que quer aceitar os seleccionados?:\r ${selectedRowsC.map(r => r.name)}?`)) {
                        this.setState({ pending: true })
                        var emails = []

                        selectedRowsC.forEach(element => {
                            emails.push(element.email)
                        })
                        approveManyCandidate(this.state.project._id, emails).then(res => { this.updateCandidateList() })

                    }

                };
                const handleDeny = () => {

                    if (window.confirm(`Tem a certeza que quer rejeitar os seleccionados?:\r ${selectedRowsC.map(r => r.name)}?`)) {
                        this.setState({ pending: true })
                        var emails = []

                        selectedRowsC.forEach(element => {
                            emails.push(element.email)
                        })
                        denyManyCandidate(this.state.project._id, emails).then(res => { this.updateCandidateList() })
                    }
                };
                return <div><Button key="Rejeitar" onClick={handleDeny} negative>Rejeitar</Button> <Button key="Aceitar" onClick={handleAccept} positive>Aceitar</Button></div>;

            }, [dataC, selectedRowsC, toggleClearedC]);


            const subHeaderComponentMemo = React.useMemo(() => {
                const handleClear = () => {
                    if (filterText) {
                        setResetPaginationToggle(!resetPaginationToggle);
                        setFilterText('');
                    }
                };

                return <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />;
            }, [filterText, resetPaginationToggle]);
            return (

                <DataTable
                    title="Lista de Candidatos"
                    columns={columns}
                    data={filteredItems}
                    defaultSortField="name"
                    fixedHeader
                    persistTableHead
                    fixedHeaderScrollHeight="300px"
                    selectableRows={this.state.email == this.state.project.project_manager}
                    highlightOnHover
                    contextActions={contextActionsC}
                    onSelectedRowsChange={handleRowSelectedC}
                    clearSelectedRows={toggleClearedC}
                    pagination
                    subHeader
                    noDataComponent="Nenhum candidato foi encontrado..."
                    subHeaderComponent={subHeaderComponentMemo}
                    selectableRowDisabled={rowDisabledCriteria}
                    disabled={this.state.termindated}
                    progressPending={this.state.pending}
                    paginationComponentOptions={paginationOptions}
                    paginationResetDefaultPage={this.state.resetPaginationToggle}
                    onRowClicked={row => {
                        this.props.history.push('/otherUser/' + row.email)
                    }}

                />
            );
        };


        const project_manager_tools = (this.state.email == this.state.project.project_manager) ?
            <Form loading={this.state.isLoading}>
                <Form.Button type="submit" disabled={this.state.project.state != "Aprovado"} onClick={this.closeConfigShow(true, false)}>Fechar Projeto</Form.Button>
                <Form.Button type="submit" disabled={this.state.project.state != "Terminado" && this.state.project.state != "Negado"} onClick={() => this.onSubmit("archive")}>Arquivar Projeto</Form.Button>
                <Button  disabled={this.state.project.state != "Terminado" && this.state.project.state != "Negado"} onClick={()=>this.exportPDF(this.state.participants)}>Descarregar Participantes do Projeto</Button>
            </Form>
            : null;
        //Closing project 
        const closingColumns = [
            {
                name: 'Nome do Candidato',
                selector: 'name',
                sortable: true,
                right: false,
            },
            {
                name: 'Email',
                selector: 'email',
                right: false,
                sortable: true,
            },


        ]
        const { open, closeOnEscape, closeOnDimmerClick } = this.state;
        const SelectableRowsManagement = () => {
            const [selectedRows, setSelectedRows] = React.useState([]);
            const [toggleCleared, setToggleCleared] = React.useState(false);
            const [data, setData] = React.useState(this.state.participants);

            const handleRowSelected = React.useCallback(state => {
                setSelectedRows(state.selectedRows);
            }, []);

            const contextActions = React.useMemo(() => {
                const handleDelete = () => {

                    if (window.confirm(`Tem a certeza que quer remover os seleccionados?:\r ${selectedRows.map(r => r.name)}?`)) {
                        selectedRows.forEach(element => {
                            this.state.quitters.push(element)
                        });

                        setToggleCleared(!toggleCleared);
                        setData(differenceBy(data, selectedRows, 'name'));
                    }
                };

                return <Button key="delete" onClick={handleDelete} negative>Remover</Button>;
            }, [data, selectedRows, toggleCleared]);

            return (
                <DataTable
                    title="Lista de Participantes"
                    columns={closingColumns}
                    data={data}
                    selectableRows
                    highlightOnHover
                    contextActions={contextActions}
                    onSelectedRowsChange={handleRowSelected}
                    clearSelectedRows={toggleCleared}
                />
            );
        };
        const modalClosingProject =
            <Modal
                open={open}
                closeOnEscape={closeOnEscape}
                closeOnDimmerClick={closeOnDimmerClick}
                onClose={this.close}
            >
                <Modal.Header>Fecho do projeto</Modal.Header>
                <Modal.Content scrolling>
                    <Message warning>
                        <Message.Header>Seleccione os inscritos que não participaram no projeto</Message.Header>
                    </Message>
                    <Form >


                        <SelectableRowsManagement></SelectableRowsManagement>


                        <br></br>
                    </Form>
                </Modal.Content>
                <Modal.Actions>

                    <Button onClick={this.close} negative>
                        Cancelar
                </Button>
                    <Button onClick={() => this.handleCloseProject()} positive>
                        Submeter fecho
                </Button>
                </Modal.Actions>
            </Modal>;





        return (
            <div>

                <div class="proj-container">
                    {modalClosingProject}
                    {awaitAprovalNote}
                    {approvedNote}
                    {rejectNote}
                    {closedNote}
                    <div class="folha">
                        <h1 class="h1">Projeto Voluntariado</h1>
                        <div class="proj-ext-info">
                            <label>Nome do Projeto</label>
                            <text>{this.state.project.project_name}</text>
                            {detailsOrgNameLabel}
                            {detailsOrgNameText}
                            <label>Estado do Projeto</label>
                            <text> {this.state.project.state} </text>
                            <label>Pessoa de Contato</label>
                            <label>{this.state.user.name}</label>
                            <label>Email</label>
                            <label>{this.state.user.email}</label>
                            <label>Telemóvel</label>
                            <label>{this.state.user.phonenumber}</label>
                            <label>Resumo do Projeto</label>
                            <label>{this.state.project.summary}</label>
                            <label>Área de Intervenção</label>
                            <label>{this.state.project.intervention_area}</label>
                            <label>Público Alvo (Beneficiários)</label>
                            <label>{this.state.project.target_audience}</label>
                            <label>Objetivos</label>
                            <label>{this.state.project.objectives}</label>
                            <label>Descrição das Atividades</label>
                            <label>{this.state.project.activity_description}</label>
                            <label>Exigência de formação específica</label>
                            <label>{this.state.project.specific_formation}</label>
                            <label>Data de inicio prevista</label>
                            <label>{this.state.project.start_date}</label>
                            <label>Data de fim prevista</label>
                            <label>{this.state.project.end_date}</label>
                            <label>Tipo de Atividades</label>
                            <label>{this.state.project.volunteer_areas}</label>
                            <label>Entidades Envolvidas</label>
                            <label>{this.state.project.involved_entities}</label>
                            <label>Observações</label>
                            <label>{this.state.project.observations}</label>
                        </div>

                    </div>

                </div>
                <div class="proj-container">
                    {project_manager_tools}

                </div>
                <div >
                    <Tabs>
                        <TabList>
                            <Tab>Inscritos</Tab>
                            {reviews}
                        </TabList>

                        <TabPanel >
                            <div class="insc-container">
                                <h1 class="h1-insc">Inscritos no Projeto</h1>

                                <SelectableCandidatesManagement></SelectableCandidatesManagement>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div class="insc-container">
                                <h1 class="h1-insc">Feedback do Projeto</h1>
                                <div>Rating global: <Rating icon='star' defaultRating={this.state.global_rating} maxRating={5} disabled /></div>
                                <DataTable
                                    title="Comentários"
                                    columns={review_columns}
                                    data={this.state.reviews}
                                    defaultSortField="name"
                                    fixedHeader
                                    fixedHeaderScrollHeight="300px"
                                    highlightOnHover
                                    pagination
                                    noDataComponent="Nenhum comentário foi feito..."
                                    expandableRows
                                    progressPending={this.state.pending}
                                    paginationComponentOptions={paginationOptions}
                                    paginationResetDefaultPage={this.state.resetPaginationToggle}
                                    expandableRowsComponent={<SampleExpandedComponent />}


                                />
                                {review_submit}
                            </div>
                        </TabPanel>
                    </Tabs>


                </div>
            </div>
        )
    }
}
export default withRouter(ProjectsDetails)