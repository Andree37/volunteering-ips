import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Message } from 'semantic-ui-react'
import { withRouter } from 'react-router';
import axios from 'axios'
import './Profile.css'
import { getUser, getOtherUser } from './UserFunctions'
import { getAllProjects } from './ProjectFunctions'

import DataTable from 'react-data-table-component'

class otherUser extends Component {
    constructor() {
        super()
        this.state = {
            name: "",
            otherUser_email: "",
            phonenumber: "",
            password: "",
            confirmPassword: "",
            is_member: false,
            type_member: "",
            birthdate: "",
            school: "",
            course: "",
            interest_areas: "",
            reason_volunteer: "",
            observations: "",
            profile_picture: "",
            resetPaginationToggle: false,
            projects: [],
            terminated_projects:[],
        }
    }


    componentDidMount() {
        this.setState({otherUser_email : localStorage.getItem('otherUser_email')})
        if (localStorage.getItem('userToken') === "" || localStorage.getItem('userToken') == null) {
            this.props.history.push('/login')
        } else {
            

            getOtherUser(localStorage.getItem('otherUser_email')).then(res => {
                if (!res.error) {
                    console.log(res)
                    this.setState({ name: res.data.result.name })
                    this.setState({ otherUser_email: localStorage.getItem('otherUser_email') })
                    this.setState({ phonenumber: res.data.result.phonenumber })
                    this.setState({ profile_picture: res.data.result.profile_picture })
                    axios.get('/users/picture/'+res.data.result.profile_picture, { responseType: 'blob' })
                    .then(res => {
                        this.setState({ profile_picture: URL.createObjectURL(res.data) });
                        getAllProjects().then(res => {
                            if (!res.error) {
                                var joined = []
                                var terminated = []
                                this.setState({ projects: joined })
                                this.setState({ terminated_projects: terminated })                             
                                res.data.result.forEach(element => {
                                    if (element.approved_candidates != null) {
                                        element.approved_candidates.forEach(candidate => {
                                            if (candidate == localStorage.getItem('otherUser_email')) {
                                                getOtherUser(element.project_manager).then(res => {
                                                    element.project_managerUser = res.data.result
                                                    
                                                    if(element.state == "Aprovado"){
                                                        joined.push(element)
                                                        this.setState({ projects: joined })
                                                    }else if(element.state == "Terminado" || element.state == "Arquivado" ){
                                                        terminated.push(element)
                                                        this.setState({ terminated_projects: terminated })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                });
                                console.log(this.state.projects)

                            }
                        })
                    })
                } else {
                    console.log(res.error)
                }
            })
               
        }
    }



    render() {
        localStorage.setItem('otherUser_email', this.props.match.params.otherUser_email)
        
        
        //Table data

        const columns = [
            {
                name: 'Nome do Projeto',
                selector: 'project_name',
                sortable: true,
            },
            {
                name: 'Nome do Gestor',
                sortable: true,
                cell: row => {
                return (<div ><Link to={'/otherUser/' + row.project_managerUser.email}> {row.project_managerUser.name}</Link></div>)
                }
            },
            {
                name: 'Estado',
                selector: 'state',
                sortable: true,
                maxWidth: "50px",
            },
            {
                name: 'Tipo',
                selector: 'inside_project',
                sortable: true,
                maxWidth: "50px",
            },
            {
                name: 'Área de Interesse',
                selector: 'volunteer_areas',
                sortable: true,
            },
            
            


        ]
        //Pagination
        const paginationOptions = { rowsPerPageText: 'Filas por página', rangeSeparatorText: 'de', selectAllRowsItem: true, selectAllRowsItemText: 'Todos' };
       
        return (
            <div class="contain">
              


                <div class="contain-form">
                    <div class="info">
                        <img class="img-perfil" alt="Imagem de perfil" src={this.state.profile_picture} />

                        <label class="info-perfil nome">Nome Completo : {this.state.name}</label>
                        <label class="info-perfil email">Email : {this.state.otherUser_email}</label>
                        <label class="info-perfil tele">Telemóvel : {this.state.phonenumber}</label>
                        
                        <div class="break"></div>

                        <div class="projetos-a-decorrer">
                            <h1>Projetos a Decorrer</h1>
                            <DataTable
                                columns={columns}
                                data={this.state.projects}
                                defaultSortField="project_name"
                        persistTableHead

                                noHeader
                                fixedHeader
                                fixedHeaderScrollHeight="100px"
                                noDataComponent="Nenhum projeto do qual participa está a decorrer..."
                                highlightOnHover
                                pagination
                                paginationComponentOptions={paginationOptions}
                                paginationResetDefaultPage={this.state.resetPaginationToggle}
                                onRowClicked={row => {
                                    this.props.history.push('/projectsdetails/' + row._id)
                                }}

                            />
                        </div>
                        <div class="projetos-terminados">
                            <h1>Projetos Terminados</h1>
                            <DataTable
                                columns={columns}
                                data={this.state.terminated_projects}
                                defaultSortField="project_name"
                                persistTableHead

                                noHeader
                                fixedHeader
                                fixedHeaderScrollHeight="100px"
                                highlightOnHover
                                pagination
                                noDataComponent="Nenhum projeto do qual participou foi terminado..."
                                paginationComponentOptions={paginationOptions}
                                paginationResetDefaultPage={this.state.resetPaginationToggle}
                                onRowClicked={row => {
                                    this.props.history.push('/projectsdetails/' + row._id)
                                }}

                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(otherUser)