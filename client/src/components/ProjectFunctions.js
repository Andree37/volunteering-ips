import axios from 'axios'

//This file is to translate API calls to functions
// pod "http://pvipsback-service.pvips-dev.svc.cluster.local/"
//local "http://localhost:8000"
//"proxy": "http://35.189.210.245:80/",

export function getAllProjects() {
    return axios
        .get('projects')
}

export function postProject(Project) {
    return axios
        .post('projects',{
            project_name: Project.project_name,
            intervention_area: Project.intervention_area,
            volunteer_areas: Project.volunteer_areas,
            summary: Project.summary,
            activity_description: Project.activity_description,
            inside_project: Project.inside_project,
            contact_person: Project.contact_person,
            target_audience: Project.target_audience,
            objectives: Project.objectives,
            org_name: Project.org_name,
            specific_formation: Project.specific_formation,
            start_date: Project.start_date,
            end_date: Project.end_date,
            involved_entities: Project.involved_entities,
            observations: Project.observations
        })
}

export function getProject(id) {
    return axios
        .get('projects/' + id)
}

// IPS internal only
export function applyToProject(id) {
    return axios
        .post('projects/' + id + '/apply')
}

//Project Manager functions
export function updateProject(id, updatedProject) {
    return axios
        .put('projects/' + id,{

        })
}
export function closeProject(id) {
    return axios
        .put('projects/' + id,{
            state:"Terminado"
        })
}

export function archiveProject(id) {
    return axios
        .delete('projects/' + id)
}

export function approveCandidate(id , email) {
    return axios
        .post('projects/' + id + '/approve_candidate',{email:email})
}

export function denyCandidate(id, email) {
    return axios
        .post('projects/' + id + '/deny_candidate' ,{email:email})
}
export function approveManyCandidate(id , emails) {
    return axios
        .post('projects/' + id + '/approve_many_candidates',{emails:emails})
}

export function denyManyCandidate(id, emails) {
    return axios
        .post('projects/' + id + '/deny_many_candidates' ,{emails:emails})
}
//Comissar functions
export function approveProject(id) {
    return axios
        .put('projects/' + id,{
            state:'Aprovado',
            approved:true
        })
}

export function denyProject(id,rejection_notice) {
    return axios
        .put('projects/' + id,{
            state:'Negado',
            approved:false,
            rejection_notice: rejection_notice,
        })
}

export function postReview(id,rating,review) {
    return axios
        .post('/projects/'+ id + '/review' ,{
            rating:rating,
            review:review
        })
}

export function postQuitter(id,email) {
    return axios
        .post('/projects/'+ id + '/quit_candidate' ,{
            email:email
        })
}
export function postManyQuitters(id,emails) {
    return axios
        .post('/projects/'+ id + '/quit_many_candidates' ,{
            emails:emails
        })
}