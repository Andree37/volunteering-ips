import React, {Component} from'react'
import './ProjectSoloGroup.css'

export default class ProjectSoloGroup extends Component{
    render(){
        return(
            <div class="container-sg">
                <div class="solo">
                    <h1>Inscrição a solo ou em grupo</h1>
                    <button>Solo</button>
                </div>
                <div class="grupo">
                    <div>
                        <input type="text"></input>
                        
                    </div>
                    <table id="grupo-tb">
                        <tr class="grupo-tb-header">
                            <th class="grupo-tb-header-tittle">Nome do Utilizador</th>
                        </tr>
                    </table>
                </div>
            </div>
        )
    }
}