import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'

import back from './img/back.png'
import './App.css'
import {
  Route,
  HashRouter
} from "react-router-dom";
import Navbar from './components/Navbar'
import Home from "./components/Home";
import Projects from "./components/Projects";
import ProjectsDetails from "./components/ProjectsDetails";
import ProjectsInscritos from "./components/ProjectsInscritos";
import ProjetoSoloGroup from "./components/ProjetoSoloGroup";
import Stats from "./components/Stats";

import Profile from "./components/Profile";
import otherUser from "./components/otherUser"
import EditPerfil from "./components/EditPerfil";
import RecoverPassword from './components/RecoverPassword'
import ResetPassword from './components/ResetPassword'
import Authorization from './components/Authorization'

import setAuth from './components/setAuth'
import Register from "./components/Register";
import Login from './components/Login'

/*
HELPFUL LINKS
 https://www.youtube.com/watch?v=BNdIOnn-wik - LOGIN, REGISTRATION, PROFILE, NAVBAR SPA
 https://react.semantic-ui.com/ - case you wanna use semantic, looks good imo

*/

class App extends Component {

  render() {
    return (
    <HashRouter>
      <div>
        <div>
          <img class="back" alt="Imagem de fundo" src={back} />
        </div>
        <div className="App">
          <Navbar />
          
            <div className="content">
              <Route exact path="/" component={Home} />
              <Route exact path="/projects" component={Projects} />
              <Route exact path="/projectsdetails/:id" component={ProjectsDetails} />
              <Route exact path="/projectsinscritos" component={ProjectsInscritos} />
              <Route exact path="/projetosologroup" component={ProjetoSoloGroup} />
              <Route exact path="/stats" component={Stats} />
              <Route exact path="/EditPerfil" component={EditPerfil} />
              <Route exact path="/otherUser/:otherUser_email" component={otherUser} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/recover" component={RecoverPassword} />
              <Route exact path="/reset_password/:token" component={ResetPassword} />
              <Route exact path="/authorization" component={Authorization} />
              <Route exact path="/auth=:token" component={setAuth} />
              <Route exact path="/profile" component={Profile} />
              <Route exact path="/admin"  />
            </div>
          
        </div>
      </div>
    </HashRouter>
    )


  }
}

export default App;
