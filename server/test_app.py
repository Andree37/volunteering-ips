"""
 Integration Tests for app

"""
import unittest
import requests
from app import app
import os
from datetime import timedelta, datetime

# To test this, app.py must be running in a different thread or the web site must be running

base_url = "http://localhost:8000/"
s = requests.Session()


def logout_test_user(tester):
    logout_url = base_url + 'users/logout/'

    tester.get(logout_url)


def login_test_user(tester):
    login_url = base_url + 'users/login/'

    tester.post(login_url, json={'email': 'test4test@gmail.com', 'password': '123'})


class UserTests(unittest.TestCase):

    # Functions for test aid
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False

        self.app = app.test_client(self)

        self.assertEqual(app.debug, False)

    # User Tests
    # Test get user's info
    def test_get_user_info_ok(self):
        tester = app.test_client(self)

        test_url = base_url + 'users/'

        login_test_user(tester)
        response = tester.get(test_url)

        self.assertEqual(response.status_code, 200)

    # Test login user normally
    def test_login_test_user_ok(self):
        tester = app.test_client(self)

        login_url = base_url + 'users/login/'
        response = tester.post(login_url, json={'email': 'test4test@gmail.com', 'password': '123'})
        self.assertEqual(response.status_code, 200)

    # General tests
    # Test main page
    def test_main(self):
        tester = app.test_client(self)
        # login
        login_test_user(tester)

        response = tester.get(base_url)

        self.assertEqual(response.status_code, 200)

    # Registration Tests
    # Test registration with existing user
    def test_register_existing_email(self):
        tester = app.test_client(self)

        test_url = base_url + 'users/register/'

        response = tester.post(test_url,
                               json={'email': 'test4test@gmail.com', 'password': '123'})
        self.assertEqual("test4test@gmail.com already exists", response.get_json().get("result").get("email"))

    # Test login with wrong password with existing user
    def test_login_wrong_data(self):
        tester = app.test_client(self)

        test_url = base_url + 'users/login/'

        response = tester.post(test_url, json={'email': 'test4test@gmail.com', 'password': '223'})
        self.assertIn("error", response.get_json())

    # Logout Tests
    # Test logout with user logged in
    def test_logout_authorized(self):
        tester = app.test_client(self)
        # login so later logout authorized
        login_test_user(tester)

        test_url = base_url + 'users/logout/'
        response = tester.get(test_url)

        self.assertEqual(response.status_code, 200)

    # Single User Tests
    # Test single_user get without logging in
    def test_single_user_unauthorized(self):
        tester = app.test_client(self)

        test_url = base_url + 'users/'
        logout_test_user(tester)
        # Logged out request
        response = tester.get(test_url)

        self.assertEqual(response.status_code, 401)

    # Test single_user put regular update
    def test_single_user_update(self):
        tester = app.test_client(self)
        test_url = base_url + 'users/'
        login_test_user(tester)
        # PUT
        # update name
        response = tester.put(test_url, json={'name': 'banana'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json().get("result").get("name"),
                         (tester.get(test_url)).get_json().get("result").get("name"))


if __name__ == "__main__":
    unittest.main()
