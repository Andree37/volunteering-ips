"""
 Flask REST application with admin, mail, login and plugins
 Made by: André Ribeiro & Daniel Afonso
"""
from abc import ABC
from datetime import datetime
from secrets import token_urlsafe
from bson.objectid import ObjectId
from flask import Flask, request, jsonify
from flask_mail import Mail, Message
from flask_pymongo import PyMongo
from flask_bcrypt import Bcrypt
from flask_admin import Admin
from flask_login import UserMixin, LoginManager, current_user, login_user, logout_user, login_required
from pymongo import ReturnDocument
from wtforms import form, fields
from flask_admin.contrib.pymongo.view import ModelView
from flask_admin.form import Select2Widget

# App initialization and configuration
app = Flask(__name__, static_url_path="/static")
# Db config
app.config['MONGO_DBNAME'] = "volunteeringdb"
app.config[
    "MONGO_URI"] = "mongodb+srv://smallboy0:bigboy123@cluster0-oad3w.gcp.mongodb.net/volunteeringdb?retryWrites=true&w=majority"
app.config['SECRET_KEY'] = "\xe7utGZI\xf6'\x95\xbe\xd1\x84\xac\xbb\xf1n"
# Mailing Config
app.config['MAIL_USERNAME'] = 'gpvolunteeremail@gmail.com'
app.config['MAIL_PASSWORD'] = 'emailbacano'
app.config['MAIL_DEFAULT_SENDER'] = 'gpvolunteeremail@gmail.com'
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_DEBUG'] = app.debug
app.config['MAIL_MAX_EMAILS'] = None
app.config['MAIL_SUPPRESS_SEND'] = app.testing
app.config['MAIL_ASCII_ATTACHMENTS'] = False
host = '35.240.109.230'
port = '80'

# Login manager
login = LoginManager(app)


# load user
@login.user_loader
def load_user(email):
    u = mongo.db.users.find_one({"email": email})
    if not u:
        return None
    if not u.get('is_auth'):
        return None
    return User(user=u)


# db initialization
mongo = PyMongo(app)
bcrypt = Bcrypt(app)
# db unique restrictions
mongo.db.users.create_index("email", unique=True)


# db Classes representation
class User(form.Form, UserMixin):
    name = fields.StringField('Name')
    email = fields.StringField('Email')
    phonenumber = fields.StringField('Phone Number')
    member = fields.StringField('Member')
    type_member = fields.StringField('Type of Member')
    birthdate = fields.StringField('Birthdate')
    school = fields.StringField('School')
    course = fields.StringField('Course')
    interest_areas = fields.StringField('Interest Areas')
    reason_volunteer = fields.StringField('Reason of volunteer')
    obs = fields.StringField('Observations')
    created_at = fields.DateField('Created At')

    def __init__(self, user, **kwargs):
        __name__ = 'Users'
        super().__init__(**kwargs)
        if user:
            self.email = user.get('email')
            self.name = user.get('name')
            self.phonenumber = user.get('phonenumber')
            self.member = user.get('is_member')
            self.type_member = user.get('type_member')
            self.birthdate = user.get('birthdate')
            self.school = user.get('school')
            self.course = user.get('course')
            self.interest_areas = user.get('interest_areas')
            self.reason_volunteer = user.get('reason_volunteer')
            self.obs = user.get('observations')
            self.profile_picture = user.get('profile_picture')
            self.login_token = user.get('login_token')
            self.is_auth = user.get('is_auth')
            self.created_at = user.get('created_at')

    def get_id(self):
        return self.email


class Project(form.Form):
    project_name = fields.StringField('Name')
    project_manager = fields.SelectField('User', widget=Select2Widget())
    state = fields.StringField('State')
    type = fields.StringField('Type')
    area = fields.StringField('Area')
    summary = fields.StringField('Summary')
    inside_project = fields.StringField('Inside Project')
    date_created = fields.StringField('Date Created')
    approved = fields.BooleanField('Approved')


# Admin intialization
admin = Admin(app)


# Add views
class UserView(ModelView):
    column_list = ('name', 'email', 'phonenumber', 'member', 'type_member',
                   'birthdate', 'school', 'course', 'interest_areas', 'reason_volunteer',
                   'obs')
    form = User


class ProjectView(ModelView):
    column_list = ('project_name', 'state', 'type', 'area', 'summary', 'inside_project', 'date_created', 'approved',
                   'project_manager_email')

    def get_list(self, *args, **kwargs):
        count, data = super(ProjectView, self).get_list(*args, **kwargs)
        # Grab user names
        users = mongo.db.users.find({})

        # Contribute user names to the models
        users_map = dict((str(x['email']), x['email']) for x in users)
        for item in data:
            item['project_manager_email'] = users_map.get(item['project_manager'])
        return count, data

    # Contribute list of user choices to the forms
    def _feed_user_choices(self, form):
        users = mongo.db.users.find({})
        form.project_manager.choices = [(str(x['email']), x['email']) for x in users]
        return form

    def create_form(self, **kwargs):
        form = super(ProjectView, self).create_form()
        return self._feed_user_choices(form)

    def edit_form(self, obj):
        form = super(ProjectView, self).edit_form(obj)
        return self._feed_user_choices(form)

    # Correct user_id reference before saving
    def on_model_change(self, form, model, is_created, **kwargs):
        user = model.get('project_manager')
        model['project_manager'] = user

        return model

    form = Project


admin.add_view(UserView(mongo.db.users, 'users'))
admin.add_view(ProjectView(mongo.db.projects, 'projects'))

# Mailing initialization
mail = Mail(app)


# User api
@app.route('/users/register/', methods=["POST"])
def register():
    users = mongo.db.users
    # nome password email phonenumber morada membro_do_ips
    # tipo_de_membro birthdate
    # escola curso_formacao
    # areas_de_interesse (varias) why_voluntario observacoes
    name = request.get_json().get('name')
    email = request.get_json().get('email')
    phonenumber = request.get_json().get('phonenumber')
    password = bcrypt.generate_password_hash(request.get_json().get('password').encode('utf-8'))
    member = request.get_json().get('is_member')
    # Caso seja membro
    type_member = request.get_json().get('type_member')
    birthdate = request.get_json().get('birthdate')
    school = request.get_json().get('school')
    course = request.get_json().get('course')
    interest_areas = request.get_json().get('interest_areas')
    reason_volunteer = request.get_json().get('reason_volunteer')
    obs = request.get_json().get('observations')

    result = ""
    inserted = False
    login_token = token_urlsafe(16)
    # insert
    try:
        user_inserted = users.insert_one({
            'name': name,
            'email': email,
            'phonenumber': phonenumber,
            'password': password,
            'is_member': member,
            'type_member': type_member,
            'birthdate': birthdate,
            'school': school,
            'course': course,
            'interest_areas': interest_areas,
            'reason_volunteer': reason_volunteer,
            'observations': obs,
            'is_auth': False,
            'login_token': login_token,
            'created_at': datetime.now()
        })
        new_user = users.find_one({'_id': user_inserted.inserted_id})

        result = {'email': new_user.get('email') + ' registered'}
        inserted = True
    except:
        result = {'email': email + ' already exists'}
    finally:
        if inserted:
            try:
                # Sending a welcome email to this user
                msg = Message("Bem vindo! GPVolunteering",
                              recipients=[email])
                msg.body = "Carregue no link para concluir o login: " + \
                           "http://" + host + ":" + port + "/#/auth=" + login_token
                mail.send(msg)
            except:
                pass
        return jsonify({'result': result})


@app.route('/users/resend_auth', methods=['POST'])
def resend_auth():
    email = request.get_json().get('email')
    user = mongo.db.users.find_one({'email': email})
    result = ""
    if user:
        try:
            # Resend email to this user
            msg = Message("Este é um email criado pelo utilizador!"
                          " Seja bem vindo! GPVolunteering",
                          recipients=[email])
            msg.body = "Carregue no link para concluir o login: " + \
                       "http://" + host + ":" + port + "/#/auth=" + user['login_token']
            mail.send(msg)
        except:
            pass
        finally:
            result = {'result': email + ' email sent to this email'}
    else:
        result = {'error': "there is no user with this email"}
    return result


@app.route('/users/login_token/<string:token>', methods=['POST'])
def login_token(token):
    updated_user = mongo.db.users.find_one_and_update({'login_token': token}, {'$set': {'is_auth': True}},
                                                      return_document=ReturnDocument.AFTER)
    if updated_user is None:
        return jsonify({'error': "Token invalid"})

    del updated_user['_id']
    del updated_user['password']
    return jsonify({'result': updated_user})


@app.route('/users/login/', methods=['POST'])
def login():
    email = request.get_json().get('email')
    password = request.get_json().get('password')
    password.encode('utf-8')

    user = mongo.db.users.find_one({'email': email})
    if user:
        if bcrypt.check_password_hash(user.get('password'), password):
            # login user
            user_obj = User(user=user)
            auth = user.get('is_auth')
            if not auth:
                result = jsonify({'error': "AuthError; " + email + " has not been authorized to use this platform"})
            else:
                login_user(user_obj)
                result = jsonify({'token': email})
        else:
            result = jsonify({"error": "Invalid username and password"})
    else:
        result = jsonify({"error": "no results found"})
    return result


@app.route('/users/logout/')
@login_required
def logout():
    logout_user()
    return jsonify({"result": "user has logged out"})


# Recover password
@app.route('/users/recover_password/', methods=['POST'])
def trigger_recover_password():
    email = request.get_json().get('email')
    token = token_urlsafe(16)
    user = mongo.db.users.find_one({'email': email})
    if user is None:
        return jsonify({'error': "email doesn't belong to a user"})
    mongo.db.users.find_one_and_update({'email': email}, {'$set': {'token': token}})

    # email user with link to recover password
    # change to front end recovery page in the future or a static page for this on the backend
    # change message body to the link that will have a form that will send a put to that url set now
    # for now user has to have a postman account xD
    try:
        # Sending a welcome email to this user
        msg = Message("Recuperar password",
                      recipients=[email])
        msg.body = "Carregue aqui para recuperar a password: " + "http://" + host + ":" + port + "/#/reset_password/" + \
                   token
        mail.send(msg)
    except:
        pass
    return jsonify({'result': "email sent to recover password"})


@app.route('/users/recover_password/<string:token>/', methods=['PUT'])
def recover_password(token):
    new_password = request.get_json().get('new_password')
    if new_password == "":
        return jsonify({'error': "password mustn't be empty"})
    new_password = bcrypt.generate_password_hash(new_password.encode('utf-8'))

    updated_user = mongo.db.users.find_one_and_update({'token': token}, {'$set': {'password': new_password}},
                                                      return_document=ReturnDocument.AFTER)
    if updated_user is None:
        return jsonify({'error': "Token invalid"})

    del updated_user['_id']
    del updated_user['password']
    return jsonify({'result': updated_user})


@app.route('/users/', methods=['GET', 'PUT'])
@login_required
def users():
    result = ""
    user = current_user
    if request.method == 'GET':
        result = {
            'name': user.name,
            'email': user.email,
            'phonenumber': user.phonenumber,
            'is_member': user.member,
            'type_member': user.type_member,
            'birthdate': user.birthdate,
            'school': user.school,
            'course': user.course,
            'interest_areas': user.interest_areas,
            'reason_volunteer': user.reason_volunteer,
            'observations': user.obs,
            'profile_picture': user.profile_picture
        }
    elif request.method == 'PUT':
        data = request.get_json()
        if 'email' in data:
            del data['email']
        if 'password' in data:
            data['password'] = bcrypt.generate_password_hash(request.get_json().get('password').encode('utf-8'))
        updated_user = mongo.db.users.find_one_and_update({'email': user.email}, {'$set': data},
                                                          return_document=ReturnDocument.AFTER)
        del updated_user['_id']
        del updated_user['password']
        result = updated_user
    return jsonify({'result': result})


@app.route('/users/<string:email>', methods=['GET'])
@login_required
def other_user(email):
    user = mongo.db.users.find_one({'email': email})
    if user:
        result = {
            'name': user.get('name'),
            'email': user.get('email'),
            'phonenumber': user.get('phonenumber'),
            'is_member': user.get('is_member'),
            'type_member': user.get('type_member'),
            'birthdate': user.get('birthdate'),
            'school': user.get('school'),
            'course': user.get('course'),
            'interest_areas': user.get('interest_areas'),
            'reason_volunteer': user.get('reason_volunteer'),
            'observations': user.get('obs'),
            'profile_picture': user.get('profile_picture')
        }
    else:
        return jsonify({'error': 'No User with this email found'})
    return jsonify({'result': result})


@app.route('/users/picture', methods=['GET', 'POST'])
@login_required
def profile_picture():
    if request.method == 'GET':
        if hasattr(current_user, 'profile_picture') and current_user.profile_picture is not None:
            return mongo.send_file(current_user.profile_picture)
        return jsonify({'result': 'no image uploaded'})
    elif request.method == 'POST':
        file = request.files['profile_picture']
        filename = current_user.email + "_" + file.filename
        mongo.save_file(filename, file)
        updated_user = mongo.db.users.find_one_and_update({'email': current_user.email},
                                                          {'$set': {'profile_picture': filename}},
                                                          return_document=ReturnDocument.AFTER)
        del updated_user['_id']
        del updated_user['password']
        return jsonify({'result': updated_user})


@app.route('/users/picture/<string:filename>', methods=['GET'])
@login_required
def other_profile_picture(filename):
    if request.method == 'GET':
        result = None
        try:
            result = mongo.send_file(filename)
        finally:
            if not result:
                return jsonify({'error': 'no image with this filename'})
            else:
                return result


# Notifications api
@app.route('/notify/', methods=['GET', 'POST', 'PUT'])
@login_required
def manage_notifications():
    notifications = mongo.db.notifications

    result = ""
    if request.method == 'GET':
        all_notifications_of_current_user = notifications.find({'user_email': current_user.email})
        unseen_notifications = []
        for notification in all_notifications_of_current_user:
            if not notification['seen']:
                del notification['_id']
                unseen_notifications.append(notification)

        count = len(unseen_notifications)
        result = {'amount': count,
                  'notifications': unseen_notifications}

    elif request.method == 'POST':
        content = request.get_json().get('content')
        notification_inserted = notifications.insert_one({
            'user_email': current_user.email,
            'seen': False,
            'content': content,
            'type': request.get_json().get('type')
        })
        new_notification = notifications.find_one({'_id': notification_inserted.inserted_id})
        del new_notification['_id']
        result = new_notification

    elif request.method == 'PUT':
        # update the seen tag of all notifications with the type received on the json
        notifications.update_many({'user_email': current_user.email,
                                   'type': request.get_json().get('type'),
                                   'seen': False}, {'$set': {'seen': True}})
        result = 'Updated all notifications of type ' + request.get_json().get('type') + ' of current user'

    return jsonify({'result': result})


@app.route('/notifyOther/', methods=['POST'])
def notify_other():
    notifications = mongo.db.notifications
    content = request.get_json().get('content')

    notification_inserted = notifications.insert_one({
        'user_email': request.get_json().get('email'),
        'seen': False,
        'content': content,
        'type': request.get_json().get('type')
    })

    new_notification = notifications.find_one({'_id': notification_inserted.inserted_id})
    del new_notification['_id']
    result = new_notification

    return jsonify({'result': result})


# Projects
@app.route('/projects', methods=['POST'])
@login_required
def all_projects():
    projects = mongo.db.projects
    result = ''

    if request.method == 'POST':
        data = request.get_json()
        project_inserted = projects.insert_one({
            'project_name': data.get('project_name'),
            'project_manager': current_user.email,
            'state': 'Por aprovar',
            'intervention_area': data.get('intervention_area'),
            'volunteer_areas': data.get('volunteer_areas'),
            'summary': data.get('summary'),
            'inside_project': data.get('inside_project'),
            'date_created': datetime.now(),
            'approved': None,
            'contact_person': data.get('contact_person'),
            'target_audience': data.get('target_audience'),
            'objectives': data.get('objectives'),
            'org_name': data.get('org_name'),
            'specific_formation': data.get('specific_formation'),
            'start_date': data.get('start_date'),
            'end_date': data.get('end_date'),
            'start_time': data.get('start_time'),
            'end_time': data.get('end_time'),
            'involved_entities': data.get('involved_entities'),
            'activity_description': data.get('activity_description'),
            'observations': data.get('observations'),
            'candidates': [current_user.email],
            'approved_candidates': [current_user.email]
        })
        new_project = projects.find_one({'_id': project_inserted.inserted_id})
        new_project['_id'] = str(new_project['_id'])

        result = new_project

    return jsonify({'result': result})


@app.route('/projects', methods=['GET'])
def all_projects_nologin():
    projects = mongo.db.projects
    result = ''

    if request.method == 'GET':
        all_projs = projects.find({})
        result_projects = []
        for proj in all_projs:
            proj['_id'] = str(proj['_id'])
            result_projects.append(proj)
            if proj.get('user_id'):
                del proj['user_id']
        result = result_projects
    return jsonify({'result': result})


@app.route('/projects/<string:id>', methods=['GET', 'PUT', 'DELETE'])
@login_required
def one_project(id):
    projects = mongo.db.projects
    result = ''

    proj = projects.find_one({'_id': ObjectId(id)})

    if proj:
        if proj.get('user_id'):
            del proj['user_id']
        if request.method == 'GET':
            proj['_id'] = str(proj['_id'])
            result = proj
        elif request.method == 'PUT':
            data = request.get_json()
            updated_project = projects.find_one_and_update({'_id': ObjectId(id)},
                                                           {'$set': data},
                                                           return_document=ReturnDocument.AFTER)
            updated_project['_id'] = str(updated_project['_id'])
            result = updated_project
        elif request.method == 'DELETE':
            deleted_project = projects.find_one_and_update({'_id': ObjectId(id)},
                                                           {'$set': {'state': 'Arquivado'}},
                                                           return_document=ReturnDocument.AFTER)
            deleted_project['_id'] = str(deleted_project['_id'])
            result = deleted_project
        return jsonify({'result': result})
    else:
        return jsonify({'error': 'There is no project with this id or the project is archived'})


@app.route('/projects/<string:id>/apply', methods=['POST'])
@login_required
def apply_project_self(id):
    projects = mongo.db.projects
    proj = projects.find_one({'_id': ObjectId(id)})

    if proj:
        if not proj.get('approved'):
            return jsonify({'result': 'Project has not been approved, please wait until it is to apply'})
        existent_candidates = proj.get('candidates')
        if not existent_candidates:
            existent_candidates = []
        # Check if this email is in the list of candidates already (same user)
        for candidate in existent_candidates:
            if candidate == current_user.email:
                return jsonify({'error': 'user already in this project'})

        existent_candidates.append(current_user.email)

        updated_project = projects.find_one_and_update({'_id': ObjectId(id)},
                                                       {'$set': {'candidates': existent_candidates}},
                                                       return_document=ReturnDocument.AFTER)
        updated_project['_id'] = str(updated_project['_id'])
        result = updated_project
    else:
        return jsonify({'error': 'no project found'})
    return jsonify({'result': result})


@app.route('/projects/<string:id>/approve_candidate', methods=['POST'])
@login_required
def approve_candidate(id):
    project = mongo.db.projects.find_one({'_id': ObjectId(id)})
    if not project:
        return jsonify({'error': 'project does not exist'})

    email = request.get_json().get('email')

    if email in project.get('approved_candidates'):
        return jsonify({'error': 'user already in approved'})

    candidates = project.get('candidates')
    approved_candidates = project.get('approved_candidates')
    if not approved_candidates:
        approved_candidates = []

    denied_candidates = project.get('denied_candidates')
    if not denied_candidates:
        denied_candidates = []

    for c in candidates:
        if email == c:
            # There's probably a better way to do this but i cant think of it now
            if email not in denied_candidates:
                if len(approved_candidates) > 0:
                    if email not in approved_candidates:
                        approved_candidates.append(email)
                else:
                    approved_candidates.append(email)

    updated_project = mongo.db.projects.find_one_and_update({'_id': ObjectId(id)},
                                                            {'$set': {'approved_candidates': approved_candidates}},
                                                            return_document=ReturnDocument.AFTER)
    updated_project['_id'] = str(updated_project['_id'])
    result = updated_project
    return jsonify({'result': result})


@app.route('/projects/<string:id>/approve_many_candidates', methods=['POST'])
@login_required
def approve_many_candidate(id):
    project = mongo.db.projects.find_one({'_id': ObjectId(id)})
    if not project:
        return jsonify({'error': 'project does not exist'})

    emails = request.get_json().get('emails')

    candidates = project.get('candidates')
    approved_candidates = project.get('approved_candidates')
    if not approved_candidates:
        approved_candidates = []

    denied_candidates = project.get('denied_candidates')
    if not denied_candidates:
        denied_candidates = []

    for e in emails:
        if e in approved_candidates:
            emails.remove(e)

    for e in emails:
        if e in candidates:
            # There's probably a better way to do this but i cant think of it now
            if e not in denied_candidates:
                if len(approved_candidates) > 0:
                    if e not in approved_candidates:
                        approved_candidates.append(e)
                else:
                    approved_candidates.append(e)

    updated_project = mongo.db.projects.find_one_and_update({'_id': ObjectId(id)},
                                                            {'$set': {'approved_candidates': approved_candidates}},
                                                            return_document=ReturnDocument.AFTER)
    updated_project['_id'] = str(updated_project['_id'])
    result = updated_project
    return jsonify({'result': result})


@app.route('/projects/<string:id>/deny_candidate', methods=['POST'])
@login_required
def deny_candidate(id):
    project = mongo.db.projects.find_one({'_id': ObjectId(id)})
    if not project:
        return jsonify({'error': 'project does not exist'})

    email = request.get_json().get('email')

    candidates = project.get('candidates')
    denied_candidates = project.get('denied_candidates')
    if not denied_candidates:
        denied_candidates = []

    approved_candidates = project.get('approved_candidates')
    if not approved_candidates:
        approved_candidates = []

    for c in candidates:
        if email == c:
            # Check if the email is in accepted candidates
            if email not in approved_candidates:
                if len(denied_candidates) > 0:
                    if email not in denied_candidates:
                        denied_candidates.append(email)
                else:
                    denied_candidates.append(email)

    updated_project = mongo.db.projects.find_one_and_update({'_id': ObjectId(id)},
                                                            {'$set': {'denied_candidates': denied_candidates}},
                                                            return_document=ReturnDocument.AFTER)
    updated_project['_id'] = str(updated_project['_id'])
    result = updated_project
    return jsonify({'result': result})


@app.route('/projects/<string:id>/deny_many_candidates', methods=['POST'])
@login_required
def deny_many_candidate(id):
    project = mongo.db.projects.find_one({'_id': ObjectId(id)})
    if not project:
        return jsonify({'error': 'project does not exist'})

    emails = request.get_json().get('emails')

    candidates = project.get('candidates')
    denied_candidates = project.get('denied_candidates')
    if not denied_candidates:
        denied_candidates = []

    for e in emails:
        if e in denied_candidates:
            emails.remove(e)

    approved_candidates = project.get('approved_candidates')
    if not approved_candidates:
        approved_candidates = []
    for e in emails:
        if e in candidates:
            # Check if the email is in accepted candidates
            if e not in approved_candidates:
                if len(denied_candidates) > 0:
                    if e not in denied_candidates:
                        denied_candidates.append(e)
                else:
                    denied_candidates.append(e)

    updated_project = mongo.db.projects.find_one_and_update({'_id': ObjectId(id)},
                                                            {'$set': {'denied_candidates': denied_candidates}},
                                                            return_document=ReturnDocument.AFTER)
    updated_project['_id'] = str(updated_project['_id'])
    result = updated_project
    return jsonify({'result': result})


@app.route('/projects/<string:id>/quit_candidate', methods=['POST'])
@login_required
def quit_candidate(id):
    project = mongo.db.projects.find_one({'_id': ObjectId(id)})
    changed = False
    if not project:
        return jsonify({'error': 'project does not exist'})

    email = request.get_json().get('email')

    approved_candidates = project.get('approved_candidates')
    quit_candidates = project.get('quit_candidates')

    if not quit_candidates:
        quit_candidates = []

    if email in approved_candidates:
        changed = True
        quit_candidates.append(email)
        approved_candidates.remove(email)
    if not changed:
        return jsonify({'error': 'candidate not in approved candidates'})

    updated_project = mongo.db.projects.find_one_and_update({'_id': ObjectId(id)},
                                                            {'$set': {'quit_candidates': quit_candidates,
                                                                      'approved_candidates': approved_candidates}},
                                                            return_document=ReturnDocument.AFTER)
    updated_project['_id'] = str(updated_project['_id'])
    result = updated_project
    return jsonify({'result': result})


@app.route('/projects/<string:id>/quit_many_candidates', methods=['POST'])
@login_required
def quit_many_candidates(id):
    project = mongo.db.projects.find_one({'_id': ObjectId(id)})
    changed = False
    if not project:
        return jsonify({'error': 'project does not exist'})

    emails = request.get_json().get('emails')

    approved_candidates = project.get('approved_candidates')
    quit_candidates = project.get('quit_candidates')
    if not quit_candidates:
        quit_candidates = []

    for e in emails:
        if e in approved_candidates:
            changed = True
            quit_candidates.append(e)
            approved_candidates.remove(e)

    if not changed:
        return jsonify({'error': 'candidate not in approved candidates'})

    updated_project = mongo.db.projects.find_one_and_update({'_id': ObjectId(id)},
                                                            {'$set': {'quit_candidates': quit_candidates,
                                                                      'approved_candidates': approved_candidates}},
                                                            return_document=ReturnDocument.AFTER)
    updated_project['_id'] = str(updated_project['_id'])
    result = updated_project
    return jsonify({'result': result})


@app.route('/projects/<string:id>/review', methods=['POST'])
@login_required
def review_project(id):
    project = mongo.db.projects.find_one({'_id': ObjectId(id)})
    if not project:
        return jsonify({'error': 'project does not exist'})
    if project.get('state') != 'Terminado':
        return jsonify({'error': 'project is not finished yet'})

    email = current_user.email
    review = request.get_json().get('review')
    rating = request.get_json().get('rating')

    reviews = project.get('reviews')
    if not reviews:
        reviews = []
    else:
        for r in reviews:
            if r.get('email') == email:
                return jsonify({'error': 'this user already made a review on this project'})

    reviews.append({'email': email, 'review': review, 'rating': rating})

    updated_project = mongo.db.projects.find_one_and_update({'_id': ObjectId(id)},
                                                            {'$set': {'reviews': reviews}},
                                                            return_document=ReturnDocument.AFTER)
    updated_project['_id'] = str(updated_project['_id'])
    result = updated_project
    return jsonify({'result': result})


@app.route('/statistics', methods=['GET'])
@login_required
def stats():
    # the collections
    users = mongo.db.users
    projects = mongo.db.projects

    # array of users and projects for the stats
    all_users = users.find({})
    all_projs = projects.find({})

    # helper variables
    today = datetime.now()
    user_ages = []
    interest = []
    intervention = []
    specific = []
    entities = []
    volunteer_area = []
    projs_duration = []
    users_per_month = []

    for u in all_users:
        # get birthdates
        if u.get('birthdate') != 'birthdate':
            date = datetime.strptime(u.get('birthdate'), '%Y-%m-%d')
            age = today.year - date.year - ((today.month, today.day) < (date.month, date.day))
            user_ages.append(age)
        # get interest areas
        if isinstance(u.get('interest_areas'), list):
            for area in u.get('interest_areas'):
                interest.append(area)
        # get avg users per month
        if isinstance(u.get('created_at'), datetime):
            users_per_month.append(u.get('created_at').month)

    for p in all_projs:
        # get intervention areas
        if isinstance(p.get('intervention_area'), str) and p.get('intervention_area') != "":
            intervention.append(p.get('intervention_area'))
        # get specific formation
        if isinstance(p.get('specific_formation'), str):
            specific.append(p.get('specific_formation'))
        # get involved entities amount
        if isinstance(p.get('involved_entities'), str) and p.get('involved_entities') != "":
            split_entities = []
            for s in p.get('involved_entities').split(';'):
                split_entities.append(s.lstrip())
            entities.extend(split_entities)
        # get volunteering areas
        if isinstance(p.get('volunteer_areas'), list):
            for area in p.get('volunteer_areas'):
                volunteer_area.append(area)
        # get avg duration of project
        if p.get('start_date') != "" and p.get('end_date') != "" and (isinstance(p.get('start_date'), str) and isinstance(p.get('end_date'), str)):
            start_date = datetime.strptime(p.get('start_date'), '%Y-%m-%d')
            end_date = datetime.strptime(p.get('end_date'), '%Y-%m-%d')
            duration = (end_date - start_date).days
            projs_duration.append(duration)

    # set return variables
    num_users = users.count_documents({})
    avg_users_age = sum(user_ages) / len(user_ages)

    all_users_age = []
    for u in user_ages:
        obj = {'type':str(u),
               'value': user_ages.count(u)}
        if obj not in all_users_age:
            all_users_age.append(obj)

    interest_areas = []
    for i in interest:
        obj = {'type': str(i),
               'value': interest.count(i)}
        if obj not in interest_areas:
            interest_areas.append(obj)

    avg_users_month = len(users_per_month) / len(set(users_per_month))

    all_users_month = []
    for u in users_per_month:
        obj = {'type': str(u),
               'value': users_per_month.count(u)}
        if obj not in all_users_month:
            all_users_month.append(obj)

    num_projects = projects.count_documents({})

    intervention_area = []
    for i in intervention:
        obj = {'type': str(i),
               'value': intervention.count(i)}
        if obj not in intervention_area:
            intervention_area.append(obj)

    specific_formation = []
    for s in specific:
        obj = {'type': str(s),
               'value': specific.count(s)}
        if obj not in specific_formation:
            specific_formation.append(obj)

    entities_envolved = []
    for e in entities:
        obj = {'type': str(e),
               'value': entities.count(e)}
        if obj not in entities_envolved:
            entities_envolved.append(obj)

    project_type = []
    for v in volunteer_area:
        obj = {'type': str(v),
               'value': volunteer_area.count(v)}
        if obj not in project_type:
            project_type.append(obj)

    avg_project_completion = sum(projs_duration) / len(projs_duration)

    # create object for return
    stats_obj = {
        'num_users': num_users,
        'users_ages': all_users_age,
        'avg_users_age': avg_users_age,
        'interest_areas': interest_areas,
        'avg_users_month': avg_users_month,
        'users_month': all_users_month,
        'num_projects': num_projects,
        'intervention_area': intervention_area,
        'specific_formation': specific_formation,
        'entities_envolved': entities_envolved,
        'project_type': project_type,
        'avg_project_completion': avg_project_completion
    }

    return jsonify({'result': stats_obj})


# Home page
@app.route('/')
def index():
    return "oi"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
